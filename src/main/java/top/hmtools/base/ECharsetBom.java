package top.hmtools.base;

/**
 * 字符编码名称与16进制表示的bom头信息
 * @author HyboJ
 *
 */
public enum ECharsetBom {
	UTF8("EFBBBF","UTF-8"),
    UTF16B("FEFF","UTF-16"),
    UTF16S("FFFE","UTF-16"),
    UTF32B("0000FEFF","UTF-32"),
    UTF32S("FFFE0000","UTF-32"),
    GB18030("84319533","GB-18030");
	
	private String bomHead;
    private String encodeName;
    
    private ECharsetBom(String bomHead,String encodeName) {
        this.bomHead=bomHead;
        this.encodeName=encodeName;
    }
    
    /**
     * <br>方法说明
     * <br>输入参数说明
     * <br>输出参数说明
     */
    @Override
    public String toString() {
        return this.bomHead;
    }

    /**
     * 获取bom头16进制字符串
     * @return the bomHead
     */
    public String getBomHead() {
        return bomHead;
    }

    /**
     * 获取字符编码名称
     * @return the encodeName
     */
    public String getEncodeName() {
        return encodeName;
    }
}
