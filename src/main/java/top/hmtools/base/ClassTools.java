package top.hmtools.base;


/**
 * class<?> 相关操作工具类
 * @author Jianghaibo
 *
 */
public class ClassTools {
	
	/**
	 * 验证一个实体类是否继承了指定的类（全限定名字符串），支持多层父级验证
	 * @param clazz    要验证的目标类
	 * @param superClassQualifiedName  一个Java类的全限定名，该类可能是要验证的目标类的父类
	 * @return
	 */
	public static boolean isExtends(Class clazz,String superClassQualifiedName){
		boolean result = false;
        //验证入参
        if(clazz == null || StringTools.isBlank(superClassQualifiedName)){
            return result;
        }
        try {
        	//获取父类
        	Class superclass = clazz.getSuperclass();
        	//如果父类不为null，则比对
        	if(superclass != null){
        		if(superClassQualifiedName.equals(superclass.getName())){
        			result = true;
        		}
        	}else{
        		result = false ;
        	}
		} catch (Exception e) {
			e.printStackTrace();
		}
        return result;
	}

    /**
     * 验证一个实体类是否实现了指定的接口（全限定名字符串），支持多层父级验证
    * 方法说明：                    isImplements
    * 输入参数说明：           @param clazz    要验证的目标类
    * 输入参数说明：           @param interfaceQualifiedName   一个Java接口的全限定名，该接口可能是要验证的目标类的父类
    * 输入参数说明：           @return
    * 输出参数说明：           boolean
    *
    *
     */
    public static boolean isImplements(Class clazz,String interfaceQualifiedName){
        boolean result = false;
        //验证入参
        if(clazz == null || StringTools.isBlank(interfaceQualifiedName)){
            return result;
        }
        try {
            //获取实现的接口
            Class[] interfaces = clazz.getInterfaces();
            for(Class csTmp:interfaces){
                //获取当前接口的全限定名称
                String currentInterfaceName = csTmp.getName();
                if(interfaceQualifiedName.equals(currentInterfaceName)){
                    result = true;
                    break;
                }else{
                    //通过递归，尝试继续向上检查
                    result = isImplements(csTmp, interfaceQualifiedName);
                    break;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }
    
}
