package top.hmtools.base;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * 字符串操作相关工具
 * 
 * @author Jianghaibo
 * 
 */
public class StringTools {

	/**
	 * 批量检查若干字符串，是否全部都为空（类似于“与”运算） isAllBlank（null,"aaa"） : false
	 * isAllBlank（null,null） : true isAllBlank（"bbb","aaa"） : false 方法说明：
	 * isAllBlank 输入参数说明： @param strs 输入参数说明： @return 输出参数说明： boolean
	 * 
	 * 
	 */
	public static boolean isAllBlank(String... strs) {
		boolean result = false;
		// 因为入参都为null了，所以都为空
		if (strs == null || strs.length < 1) {
			result = true;
			return result;
		}

		// 遍历，只要其中有一个不为空，则返回false。
		for (String str : strs) {
			if (isNotBlank(str)) {
				return result;
			}
		}
		result = true;
		return result;
	}

	/**
	 * 批量检查若干字符串，是否至少1个元素为空 方法说明： isAnyBlank 输入参数说明： @param strs 输入参数说明： @return
	 * 输出参数说明： boolean
	 * 
	 * 
	 */
	public static boolean isAnyBlank(String... strs) {
		// 因为入参都为null了，所以都为空
		if (strs == null || strs.length < 1) {
			return true;
		}
		// 遍历，只要其中有一个为空，则返回true。
		for (String str : strs) {
			if (isBlank(str)) {
				return true;
			}
		}
		// 全都不为空
		return false;
	}

	/**
	 * 批量检查若干字符串，全部都为空（类似于“与”运算），则返回结果为：true <br>
	 * 方法说明： isBlank 输入参数说明： @param strs 输入参数说明： @return 输出参数说明： boolean
	 * 
	 * 
	 */
	public static boolean isBlank(String str, String... strs) {
		boolean result = isBlank(str);
		for (String tmp : strs) {
			result = result & isBlank(tmp);
			if (!result)
				break;
		}
		return result;
	}

	/**
	 * <p>
	 * 检查字符串是否为空
	 * </p>
	 * 
	 * <pre>
	 * StringTools.isBlank(null)      = true
	 * StringTools.isBlank("")        = true
	 * StringTools.isBlank(" ")       = true
	 * StringTools.isBlank("bob")     = false
	 * StringTools.isBlank("  bob  ") = false
	 * </pre>
	 * 
	 * @param str
	 * @return <code>true</code> 字符串为空
	 */
	public static boolean isBlank(String str) {
		int strLen;
		if (str == null || (strLen = str.length()) == 0) {
			return true;
		}
		for (int i = 0; i < strLen; i++) {
			if ((Character.isWhitespace(str.charAt(i)) == false)) {
				return false;
			}
		}
		return true;
	}

	/**
	 * 检查字符传是否不为空
	 * 
	 * <pre>
	 * StringTools.isNotBlank(null)      = false
	 * StringTools.isNotBlank("")        = false
	 * StringTools.isNotBlank(" ")       = false
	 * StringTools.isNotBlank("bob")     = true
	 * StringTools.isNotBlank("  bob  ") = true
	 * </pre>
	 * 
	 * @param str
	 * @return
	 */
	public static boolean isNotBlank(String str) {
		return !StringTools.isBlank(str);
	}

	/**
	 * 批量检查若干字符串，全部都不为空（类似于“与”运算），则返回结果为：true <br>
	 * 方法说明： isNotBlank 输入参数说明： @param strs 输入参数说明： @return 输出参数说明： boolean
	 * 
	 * 
	 */
	public static boolean isNotBlank(String str, String... strs) {
		boolean result = isNotBlank(str);
		for (String tmp : strs) {
			result = result & isNotBlank(tmp);
			if (!result)
				break;
		}
		return result;
	}

	public static final char UNDERLINE = '_';

	/**
	 * 驼峰格式字符串转换为下划线格式字符串
	 * 
	 * @param param
	 * @return
	 */
	public static String camelToUnderline(String param) {
		if (param == null || "".equals(param.trim())) {
			return "";
		}
		int len = param.length();
		StringBuilder sb = new StringBuilder(len);
		for (int i = 0; i < len; i++) {
			char c = param.charAt(i);
			if (Character.isUpperCase(c)) {
				sb.append(UNDERLINE);
				sb.append(Character.toLowerCase(c));
			} else {
				sb.append(c);
			}
		}
		return sb.toString();
	}

	/**
	 * 下划线格式字符串转换为驼峰格式字符串
	 * <br>app___version_________fld  ==> app_Version____Fld
	 * @param param
	 * @return
	 */
	public static String underlineToCamel(String param) {
		if (param == null || "".equals(param.trim())) {
			return "";
		}
		int len = param.length();
		StringBuilder sb = new StringBuilder(len);
		for (int i = 0; i < len; i++) {
			char c = param.charAt(i);
			if (c == UNDERLINE) {
				if (++i < len) {
					sb.append(Character.toUpperCase(param.charAt(i)));
				}
			} else {
				sb.append(c);
			}
		}
		return sb.toString();
	}

	/**
	 * 使用正则表达式将下划线格式字符串转换为驼峰格式字符串
	 * <br>app___version_________fld  ==> appVersionFld
	 * @param param
	 * @return
	 */
	public static String underlineToCamelByRegex(String param) {
		if (param == null || "".equals(param.trim())) {
			return "";
		}
		StringBuilder sb = new StringBuilder(param);
		Matcher mc = Pattern.compile("_").matcher(param);
		int i = 0;
		while (mc.find()) {
			int position = mc.end() - (i++);
			sb.replace(position - 1, position + 1,
					sb.substring(position, position + 1).toUpperCase());
		}
		return sb.toString();
	}

	/**
	 * 使用正则表达式将下划线格式字符串转换为驼峰格式字符串，并且首字母大写
	 * <br>app___version_________fld  ==> AppVersionFld
	 * @param param
	 * @return
	 */
	public static String underlineToCamelByRegexAndUpcaseFirstChar(String param) {
		String result = underlineToCamelByRegex(param);
		if (StringTools.isBlank(result)) {
			return result;
		}
		result = result.replaceFirst(result.substring(0, 1),
				result.substring(0, 1).toUpperCase());
		return result;
	}
	
	/**
	 * 首字母转大写
	 * @param str
	 * @return
	 */
	public static String upcaseFirst(String str){
		if(StringTools.isBlank(str)){
			return str;
		}
		return str.replaceFirst(str.substring(0, 1), str.substring(0, 1).toUpperCase());
	}
	
	/**
	 * 根据正则表达式，提取文本中所有符合的数据集合
	 * @param str
	 * @param regex
	 * @return
	 */
	public static List<String> distillRegex(String str, String regex) {
		List<String> result = new ArrayList<String>();
		if(StringTools.isAnyBlank(str,regex)){
			return result;
		}
		
		// 创建 Pattern 对象
		Pattern pattern = Pattern.compile(regex);
		
		Matcher matcher = pattern.matcher(str);
		while(matcher.find()){
			int start = matcher.start();
			int end = matcher.end();
			String tmp = str.substring(start, end);
			result.add(tmp);
		}
		return result;
	}
	
	/**
	 * 去掉字符串符合指定的后缀
	 * <br>str:"aaaaaa....."
	 * <br>suffix:"."
	 * <br>==> "aaaaaa"
	 * @param str
	 * @param suffix
	 * @return
	 */
	public static String trimSuffix(String str,String suffix){
		if(StringTools.isAnyBlank(str,suffix)){
			return str;
		}
		String result = null;
    	if(str.endsWith(suffix)){
    		int lastIndexOf = str.lastIndexOf(suffix);
    		result = str.substring(0,lastIndexOf);
    		result = trimSuffix(result, suffix);
    	}else{
    		result =  str;
    	}
    	return result;
	}
}
