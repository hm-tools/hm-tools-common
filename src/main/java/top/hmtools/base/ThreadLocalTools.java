package top.hmtools.base;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map.Entry;
import java.util.Set;

/**
 * ThreadLocal工具，用于线程中传递参数数据
 * @author HyboJ
 *
 */
public class ThreadLocalTools {

	private static final ThreadLocal<HashMap<Object, Object>> TL = new ThreadLocal<HashMap<Object, Object>>();
	
	/**
	 * 添加数据集合
	 * @param items
	 */
	public static <K,V> void put(Entry<K, V>... items){
		init();
		if(items == null || items.length<1){
			return;
		}
		for(Entry<K, V> item:items){
			TL.get().put(item.getKey(), item.getValue());
		}
	}
	
	/**
	 * 添加一条数据
	 * @param k
	 * @param v
	 */
	public static <K,V> void put(K k,V v){
		init();
		if(k==null){
			return;
		}
		TL.get().put(k, v);
	}
	
	/**
	 * 获取一条数据
	 * @param key
	 * @return
	 */
	public static Object get(Object key){
		return get(key,Object.class);
	}
	
	/**
	 * 获取一条数据
	 * @param k
	 * @param v
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public static <K,V>V get(K k,Class<V> v){
		init();
		if(k==null){
			return null;
		}
		Object object = TL.get().get(k);
		if(object == null){
			return null;
		}else{
			return (V)object;
		}
	}
	
	/**
	 * 移除一条数据
	 * @param k
	 */
	public static <K> void remove(K k){
		init();
		if(k==null){
			return;
		}
		TL.get().remove(k);
	}
	
	/**
	 * 获取set
	 * @return
	 */
	public static Set<Entry<Object, Object>> getSet(){
		init();
		return TL.get().entrySet();
	}
	
	/**
	 * 是否存在指定key名的元素
	 * @param key
	 * @return
	 */
	public static boolean containsKey(Object key){
		init();
		return TL.get().containsKey(key);
	}

	/**
	 * 获取所有key
	 * @return
	 */
	public static Set<Object> keySet(){
		init();
		return TL.get().keySet();
	}

	/**
	 * 获取所有value
	 * @return
	 */
	public static Collection<Object> values(){
		init();
		return TL.get().values();
	}

	/**
	 * 是否为空
	 * @return
	 */
	public static boolean isEmpty(){
		init();
		return TL.get().isEmpty();
	}
	
	/**
	 * 获取所有数据条目数
	 * @return
	 */
	public static int size(){
		init();
		return TL.get().size();
	}

	/**
	 * 清空
	 */
	public static  void clear(){
		init();
		TL.get().clear();
	}
	
	private static void init(){
		HashMap<Object,Object> map = TL.get();
		if(map == null){
			map = new HashMap<Object, Object>();
			TL.set(map);
		}
	}
}
