package top.hmtools.enums;

/**
 * yy-MM-dd HH:mm:ss SSS am
 */
/**
 * 日期格式枚举
 * @author HyboJ
 *
 */
public enum EDateFormat {

    /**
     * yyyyMMddHHmmssSSS
     */
    yyyyMMddHHmmssSSS("yyyyMMddHHmmssSSS"),
    
	/**
	 * yyyy-MM-dd
	 */
	yyyyMMdd_hg("yyyy-MM-dd"),
	
	/**
	 * yyyy-MM-dd HH:mm:ss
	 */
	yyyyMMdd_hg_HHmmss_m("yyyy-MM-dd HH:mm:ss"),
	
	/**
	 * yyyy-MM-dd HH:mm:ss:SSS
	 */
	yyyyMMdd_hg_HHmmssSSS_m("yyyy-MM-dd HH:mm:ss:SSS"),
	
	/**
	 * yyyy-MM-dd HH:mm:ss SSS
	 */
	yyyyMMdd_hg_HHmmss_m_SSS("yyyy-MM-dd HH:mm:ss SSS"),
	
	/**
	 * yyyy/MM/dd
	 */
	yyyyMMdd_zx("yyyy/MM/dd"),
	
	/**
	 * yyyy年MM月dd日
	 */
	yyyyMMdd_nyr("yyyy年MM月dd日"),

	/**
	 * yyyy年MM月dd日 HH时mm分ss秒
	 */
	yyyyMMdd_nyr_HHmmss_sfm("yyyy年MM月dd日 HH时mm分ss秒"),
	;
	
	private String formatStr;
	
	/**
	 * 构造函数
	 * @param formatStr
	 */
	EDateFormat(String formatStr){
		this.formatStr = formatStr;
	}
	
	@Override
	public String toString() {
		return this.formatStr;
	}
}
