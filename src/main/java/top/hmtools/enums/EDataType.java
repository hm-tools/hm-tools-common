package top.hmtools.enums;
/**
 * 数据类型枚举
 * @author Jianghaibo
 *
 */
public enum EDataType {
    /**
     *字节 
     */
    Byte("Byte"),
    
    /**
     * 短整型
     */
    Short("Short"),
    
    /**
     * 长整型
     */
    Long("Long"),
    
    /**
     * 整型
     */
    Integer("Integer"),
    
    /**
     * 布尔
     */
    Boolean("Boolean"),
    
    /**
     * 字符
     */
    Char("Char"),
    
    /**
     * 字符串
     */
    String("String"),
    
    /**
     * 单精度浮点数
     */
    Float("Float"),
    
    /**
     * 双精度浮点数
     */
    Double("Double"),
    
    /**
     * 对象结构
     */
    Object("Object"),
    
    /**
     * 缺省空白字符串
     */
    Default("")
    ;

    private String dataTypeName;
    
    EDataType(String dataTypeName){
        this.dataTypeName = dataTypeName;
    }
}
