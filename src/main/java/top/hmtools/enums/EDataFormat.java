package top.hmtools.enums;
/**
 * 数据格式枚举
 * @author Jianghaibo
 *
 */
public enum EDataFormat {

    /**
     * xml
     */
    XML("XML"),
    
    /**
     * json
     */
    JSON("JSON"),
    
    /**
     * JSONP
     */
    JSONP("JSONP"),
    
    /**
     * TEXT
     */
    TEXT("TEXT"),
    
    /**
     * HTML
     */
    HTML("HTML"), 
    
    /**
     * HTML_FORM
     */
    HTML_FORM("HTML_FORM"),
    
    /**
     * 缺省 空字符串
     */
    DEFAULT("");
    
    private String dataTypeName;
    
    EDataFormat(String dataTypeName){
        this.dataTypeName = dataTypeName;
    }
}
