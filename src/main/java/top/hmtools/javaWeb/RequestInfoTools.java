package top.hmtools.javaWeb;

import java.lang.reflect.Method;
import java.util.Enumeration;

import top.hmtools.base.ClassTools;
import top.hmtools.model.ResultModel;

/**
 * 获取request请求中的信息工具
 * @author Jianghaibo
 *
 */
public class RequestInfoTools {
	
	private static final String INTERFACE_REQUEST_NAME = "javax.servlet.ServletRequest";
	
	
	/**
	 * 获取HTTP请求头部信息数据集合
	 * @param obj
	 * @return
	 */
	@SuppressWarnings("unchecked")
    public static ResultModel getRequestHeaderInfo(Object obj){
		ResultModel result = new ResultModel();
        try {
        	//验证是否是 request
        	boolean isImplementsServletRequest = ClassTools.isImplements(obj.getClass(), INTERFACE_REQUEST_NAME);
            if(!isImplementsServletRequest){
                result.setMessage("该对象实例并不是"+INTERFACE_REQUEST_NAME+"的子类（实现类）");
                return result;
        	}
        	
        	//通过反射获取HTTP请求头信息集合
        	Method methodGetHeaderNames = obj.getClass().getMethod("getHeaderNames", Enumeration.class);
        	Method methodGetHeaderValue = obj.getClass().getMethod("getHeader", String.class);
        	Enumeration<String> headerNames = (Enumeration<String>)methodGetHeaderNames.invoke(obj);
        	while(headerNames.hasMoreElements()){
        		String headerName = headerNames.nextElement();
        		String headerValue = (String)methodGetHeaderValue.invoke(obj,headerName);
        		result.put(headerName, headerValue);
        	}
        	result.setStatusSuccess();
        } catch (Exception e) {
            result.setMessage(e.getMessage());
        }
        return result;
	}

}
