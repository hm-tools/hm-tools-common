package top.hmtools.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * 基于hashmap的结果数据模型
 * @author Jianghaibo
 */
@SuppressWarnings("rawtypes")
public class ResultModel<T extends Object> extends HashMap implements Serializable{
    
    /**
     * 
     */
    private static final long serialVersionUID = 915305885727653538L;

    /**
     * 成功状态
     */
    static Integer STATUS_SUCCESS = resultStatus.STATUS_SUCCESS.toInt();
    
    /**
     * 失败状态
     */
    static Integer STATUS_FAIL = resultStatus.STATUS_FAIL.toInt();
    
    /**
     * 结果状态键名
     */
    public final static String KEY_STATUS = keyNames.KEY_STATUS.toString();
    
    /**
     * 反馈消息键名
     */
    final static String KEY_MESSAGE = keyNames.KEY_MESSAGE.toString();
    
    /**
     * 数据集合
     */
    final static String KEY_ROWS = keyNames.KEY_ROWS.toString();
    
    /**
     * 缺省的构造函数，状态值为“0”（STATUS_FAIL），
     */
    public ResultModel(){
        this(resultStatus.STATUS_FAIL);
    }

    /**
     * 指定状态值的构造函数
     * <br>top.hmtools.model.ResultModelHashmap.STATUS_SUCCESS ：成功状态
     * <br>top.hmtools.model.ResultModelHashmap.STATUS_FAIL：失败状态
     */
    @SuppressWarnings("unchecked")
    public ResultModel(int status){
        super();
        super.put( KEY_STATUS, status);
        super.put( KEY_MESSAGE, "");
    }

    /**
     * 指定状态值的构造函数
     */
    public ResultModel(resultStatus status){
        this(status.toInt());
    }

    /**
     * 设置列表集合数据
    * 方法说明：                    setRows
    * 输入参数说明：           @param rows
    * 输出参数说明：           void
    *
    *
     */
    @SuppressWarnings("unchecked")
	public void setRows(List<T> rows){
        super.put(KEY_ROWS, rows);
    }
    
    /**
	 * 设置列表集合数据
	 * @param rows
	 */
	public  void setRows(T... rows){
		if(rows == null || rows.length < 1){
			return;
		}
		List<T> list = new ArrayList<T>();
		for(T tt:rows){
			if(tt != null){
				list.add(tt);
			}
		}
		this.setRows(list);
	}

	/**
	 * 设置列表集合数据
	 * @param rows
	 */
	@Deprecated
	public void sSetRows(T...rows){
		this.setRows(rows);
	}

	/**
     * 追加数据到列表数据集
     * @param rows
     */
    @SuppressWarnings("unchecked")
	public void addToRows(T...rows){
    	if(rows == null || rows.length < 1){
    		return;
    	}
    	Object rowsObj = super.get(KEY_ROWS);
    	List<T> list = null;
    	if(rowsObj == null){
    		list = new ArrayList<T>();
    		super.put(KEY_ROWS, list);
    	}else{
    		list = (List<T>)rowsObj;
    	}
    	for(T tt:rows){
    		if(tt != null){
    			list.add(tt);
    		}
    	}
    }
    
    /**
     * 获取列表集合数据
    * 方法说明：                    getRows
    * 输入参数说明：           @return
    * 输出参数说明：           List
    *
    *
     */
    @SuppressWarnings("unchecked")
	public List<T> getRows(){
        return (List<T>)super.get(KEY_ROWS);
    }
    
    /**
     * 获取列表集合数据
     * @param type
     * @return
     */
    @SuppressWarnings("unchecked")
	public List<T> getRows(Class<T> type){
    	return (List<T>)super.get(KEY_ROWS);
    }
    
    
    
    /**
     * 根据key获取映射中的值
     * @param key
     * @param type
     * @return
     */
    @SuppressWarnings("unchecked")
	public <TT extends Object>TT get(Object key,Class<TT> type){
    	Object object = super.get(key);
    	if(object == null){
    		return null;
    	}else{
    		return (TT)object;
    	}
    }
    
    /**
     * 根据key获取映射中的值
     * @param key
     * @param type
     * @return
     */
    @SuppressWarnings("unchecked")
    public <TT extends Object>TT get(keyNames key,Class<TT> type){
    	Object object = super.get(key.toString());
    	if(object == null){
    		return null;
    	}else{
    		return (TT)object;
    	}
    }

    /**
     * 设置执行结果状态
    * 方法说明：                    setStatus
    * 输入参数说明：           @param status
    * 输出参数说明：           void
    *
    *
     */
    @SuppressWarnings("unchecked")
    public void setStatus(resultStatus status){
        super.put( KEY_STATUS, status.toString());
    }
    
    /**
     * 获取执行结果状态
    * 方法说明：                    getStatus
    * 输入参数说明：           @return
    * 输出参数说明：           String
    *
    *
     */
    public String getStatus(){
        return String.valueOf(super.get(KEY_STATUS));
    }
    
    /**
     * 是否是执行成功状态
     * @return
     */
    public boolean isStatusSuccess(){
    	return String.valueOf(STATUS_SUCCESS).equals(String.valueOf(super.get(KEY_STATUS)));
    }
    
    /**
     * 是否是执行失败状态
     * @return
     */
    public boolean isStatusFail(){
    	return !this.isStatusSuccess();
    }
    
    /**
     * 标记执行状态为 成功
    * 方法说明：                    statusSuccess
    * 输入参数说明：           
    * 输出参数说明：           void
    *
    *
     */
    @SuppressWarnings("unchecked")
    public void setStatusSuccess(){
        super.put( KEY_STATUS,  STATUS_SUCCESS);
    }
    
    /**
     * 标记执行状态为 失败
    * 方法说明：                    statusFail
    * 输入参数说明：           
    * 输出参数说明：           void
    *
    *
     */
    @SuppressWarnings("unchecked")
    public void setStatusFail(){
        super.put( KEY_STATUS,  STATUS_FAIL);
    }
    
    /**
     * 修改反馈消息
    * 方法说明：                    message
    * 输入参数说明：           @param msg
    * 输出参数说明：           void
    *
    *
     */
    @SuppressWarnings("unchecked")
    public void setMessage(String msg){
        super.put( KEY_MESSAGE,  msg);
    }
    
    /**
     * 获取反馈消息
    * 方法说明：                    getMessage
    * 输入参数说明：           @return
    * 输出参数说明：           String
    *
    *
     */
    public String getMessage(){
        return (String) super.get(KEY_MESSAGE);
    }
    
    /**
     * 本实体类中的常用键名称
     * @author Jianghaibo
     *
     */
    public enum keyNames{
        /**
         * 执行结果状态键名
         */
        KEY_STATUS("status"),
        /**
         * 执行结果反馈消息键名
         */
        KEY_MESSAGE("message"),
        /**
         * 执行结果通用数据集键名
         */
        KEY_ROWS("rows");
        
        /**
         * 键名称
         */
        private String keyName;
        
        /**
         * 类似于构造函数
         */
        private keyNames(String keyName){
            this.keyName = keyName;
        }
        
        /**
         * 重写tostring方法
        * 方法说明：                    toString
        * 输入参数说明：           @return
        * 输出参数说明：           String
        *
        *
         */
        @Override
        public String toString(){
            return this.keyName;
        }
    }
    
    /**
     * 处理结果状态枚举
     * @author Jianghaibo
     *
     */
    public enum resultStatus {
        /**
         * 失败状态
         */
        STATUS_FAIL(0),
        /**
         * 成功状态
         */
        STATUS_SUCCESS(1);
        /**
         * 定义枚举常量的代号值
         */
        private int code;
        
        /**
         * 可以理解成构造函数
         */
        private resultStatus(int code){
            this.code=code;
        }
        
        /**
         * 返回状态代号
        * 方法说明：                    toInt
        * 输入参数说明：           @return
        * 输出参数说明：           int
        *
        *
         */
        public int toInt(){
            return this.code;
        }
        
        /**
         * 
         * 方法说明  重写toString（）方法，这样，resultStatus.STATUS_SUCCESS 的打印值为 “1”
         * 输入参数说明
         * 输出参数说明
         */
        @Override  
        public String toString() {  
            return String.valueOf(code);  
        } 
    }
}
