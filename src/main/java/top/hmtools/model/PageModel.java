package top.hmtools.model;

import java.io.Serializable;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/**
 * 分页模型实体类
 * @author HyboJ
 * 创建日期：2017-1-12下午1:32:16
 */
public class PageModel implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 805872895554443997L;

	/**
	 * 总条目数，初始化需要
	 */
	private int totalItem=0;
	
	/**
	 * 总页数
	 */
	private int totalPages;
	
	/**
	 * 每页的总条目数
	 */
	private int pageSize = 20;
	
	/**
	 * 当前页码，初始化需要
	 */
	private int currPageNo = 1;
	
	/**
	 * 上一页的 页码
	 */
	private int prePageNo;
	
	/**
	 * 下一页的 页码
	 */
	private int nextPageNo;
	
	/**
	 * 首页页码
	 */
	private int firstPageNo = 1;
	
	/**
	 * 末页页码
	 */
	private int endPageNo;
	
	/**
	 * 当前页码中条目的起始条目排序号
	 */
	private int startItemNo;
	
	/**
	 * 当前页码中条目的结束条目排序号
	 */
	private int endItemNo;
	
	/**
	 * 当前页是否是首页
	 */
	private boolean isFirstPageNo;
	
	/**
	 * 当前页是否是末页
	 */
	private boolean isEndPageNo;
	
	/**
	 * 是否有上一页
	 */
	private boolean isHasPrePageNo;
	
	/**
	 * 是否有下一页
	 */
	private boolean isHasNextPageNo;
	
	/**
	 * 上一页最后一条记录的ID
	 */
	private String lastPageEndId;
	
	/**
	 * 构造函数初始化，指定总条目数，当前页码，每页总条目数
	 * @param totalItem	总条目数，初始化需要
	 * @param currPageNo	当前页码，初始化需要
	 * @param pageSieze		每页的总条目数
	 */
	public PageModel(int totalItem,int currPageNo,int pageSieze){
		this.init(totalItem, currPageNo, pageSieze);
	}
	
	/**
	 * 构造函数初始化，指定总条目数，当前页码，使用缺省每页总条目数（20条）
	 * @param totalItem 总条目数，初始化需要
	 * @param currPageNo	当前页码，初始化需要
	 */
	public PageModel(int totalItem,int currPageNo){
		this.init(totalItem, currPageNo);
	}
	
	/**
	 * 构造函数初始化，指定总条目数，使用缺省当前页码（第1页），使用缺省每页总条目数（20条）
	 * @param totalItem
	 */
	public PageModel(int totalItem){
		this.init(totalItem);
	}
	
	public PageModel(){
		this.init();
	}
	
	/**
	 * 初始化 分页模型对象实例
	 * @param totalItem
	 * @param currPageNo
	 * @param pageSize
	 */
	public void init(int totalItem,int currPageNo,int pageSize){
		this.pageSize = pageSize;
		this.init(totalItem, currPageNo);
	}
	
	/**
	 * 初始化 分页模型对象实例
	 * @param totalItem
	 * @param currPageNo
	 */
	public void init(int totalItem,int currPageNo){
		this.currPageNo = currPageNo;
		this.init(totalItem);
	}
	
	/**
	 * 初始化 分页模型对象实例
	 * @param totalItem
	 */
	public void init(int totalItem){
		this.totalItem = totalItem;
		this.init();
	}
	
	/**
	 * 初始化 分页模型对象实例
	 */
	public void init(){
		
		//计算总页数
		if(this.pageSize<1){
			this.pageSize=20;
		}
		if(this.totalItem<=0){
			this.totalPages=1;
		}else{
			int pageNoTmp = this.totalItem/this.pageSize;
			int moTmp = this.totalItem%this.pageSize;
			if(moTmp>0){
				pageNoTmp++;
			}
			
			this.totalPages = pageNoTmp;
		}
		
		//处理当前页码的合法性
		if(this.currPageNo<1){
			this.currPageNo=1;
		}
		if(this.currPageNo>this.totalPages){
			this.currPageNo=this.totalPages;
		}
		
		//计算上一页页码
		if(this.totalPages >= 1){
			if(this.currPageNo > 1){
				this.prePageNo = this.currPageNo-1;
			}else{
				this.prePageNo = 1;
			}
		}else{
			this.prePageNo = 1;
		}
		
		//计算下一页页码
		if(this.totalPages >= 1){
			if(this.currPageNo < this.totalPages){
				this.nextPageNo = this.currPageNo +1;
			}else{
				this.nextPageNo = this.totalPages;
			}
		}else{
			this.nextPageNo = 1;
		}
		
		//计算第一页页码
		this.firstPageNo = 1;
		
		//计算末页页码
		this.endPageNo = this.totalPages;
		
		//计算当前页起始条目序号
		if(this.currPageNo <= 1){
			this.startItemNo = 0;
		}else{
			this.startItemNo = (this.currPageNo-1) * this.pageSize;
		}
		
		//计算当前页结束条目序号
		if(this.currPageNo == this.totalPages){
			int moTmp = this.totalItem%this.pageSize;
			if(moTmp==0){
				this.endItemNo = this.startItemNo + this.pageSize;
			}else{
				this.endItemNo = this.startItemNo + moTmp-1;
			}
		}else{
			this.endItemNo = this.startItemNo+this.pageSize-1;
		}
		
		//计算当前页是否是首页
		if(this.currPageNo == 1){
			this.isFirstPageNo = true;
		}else{
			this.isFirstPageNo = false;
		}
		
		//计算当前页是否是末页
		if(this.currPageNo == this.totalPages){
			this.isEndPageNo = true;
		}else{
			this.isEndPageNo = false;
		}
		
		//计算是否有上一页
		if(this.totalPages>1){
			if(this.currPageNo == 1){
				this.isHasPrePageNo = false;
			}else{
				this.isHasPrePageNo = true;
			}
		}else{
			this.isHasPrePageNo = false;
		}
		
		//计算是否有下一页
		if(this.totalPages>1){
			if(this.currPageNo == this.totalPages){
				this.isHasNextPageNo = false;
			}else{
				this.isHasNextPageNo = true;
			}
		}else{
			this.isHasNextPageNo = false;
		}
		
	}

	/**
	 * 获取总条目数
	 * @return
	 */
	public int getTotalItem() {
		return totalItem;
	}

	/**
	 * 总条目数
	 * @param totalItem
	 */
	public PageModel setTotalItem(int totalItem) {
		this.totalItem = totalItem;
		return this;
	}

	/**
	 * 总页数
	 * @return
	 */
	public int getTotalPages() {
		return totalPages;
	}


	/**
	 * 每页的总条目数
	 * @return
	 */
	public int getPageSize() {
		return pageSize;
	}

	/**
	 * 每页的总条目数
	 * @param pageSize
	 * @return 
	 */
	public PageModel setPageSize(int pageSize) {
		this.pageSize = pageSize;
		return this;
	}

	/**
	 * 当前页码
	 * @return
	 */
	public int getCurrPageNo() {
		return currPageNo;
	}

	/**
	 * 当前页码
	 * @param currPageNo
	 * @return 
	 */
	public PageModel setCurrPageNo(int currPageNo) {
		this.currPageNo = currPageNo;
		return this;
	}

	/**
	 * 上一页的 页码
	 * @return
	 */
	public int getPrePageNo() {
		return prePageNo;
	}


	/**
	 * 下一页的 页码
	 * @return
	 */
	public int getNextPageNo() {
		return nextPageNo;
	}


	/**
	 * 首页页码
	 * @return
	 */
	public int getFirstPageNo() {
		return firstPageNo;
	}


	/**
	 * 末页页码
	 * @return
	 */
	public int getEndPageNo() {
		return endPageNo;
	}


	/**
	 * 当前页码中条目的起始条目排序号
	 * <br>以“0”表示第一条记录
	 * @return
	 */
	public int getStartItemNo() {
		return startItemNo;
	}


	/**
	 * 当前页码中条目的结束条目排序号
	 * <br>以“0”表示第一条记录
	 * @return
	 */
	public int getEndItemNo() {
		return endItemNo;
	}


	/**
	 * 当前页是否是首页
	 * @return
	 */
	public boolean isFirstPageNo() {
		return isFirstPageNo;
	}
	
	/**
	 * 当前页是否是首页
	 * @return
	 */
	public boolean getIsFirstPageNo(){
		return isFirstPageNo;
	}


	/**
	 * 当前页是否是末页
	 * @return
	 */
	public boolean isEndPageNo() {
		return isEndPageNo;
	}
	
	/**
	 * 当前页是否是末页
	 * @return
	 */
	public boolean getIsEndPageNo() {
		return isEndPageNo;
	}


	/**
	 * 是否有上一页
	 * @return
	 */
	public boolean isHasPrePageNo() {
		return isHasPrePageNo;
	}
	
	/**
	 * 是否有上一页
	 * @return
	 */
	public boolean getIsHasPrePageNo() {
		return isHasPrePageNo;
	}


	/**
	 * 
		是否有下一页
	 * @return
	 */
	public boolean isHasNextPageNo() {
		return isHasNextPageNo;
	}
	
	/**
	 * 
		是否有下一页
	 * @return
	 */
	public boolean getIsHasNextPageNo() {
		return isHasNextPageNo;
	}


	/**
	 * 上一页最后一条记录的ID
	 * @return
	 */
	public String getLastPageEndId() {
		return lastPageEndId;
	}

	/**
	 * 上一页最后一条记录的ID
	 * @param lastPageEndId
	 */
	public void setLastPageEndId(String lastPageEndId) {
		this.lastPageEndId = lastPageEndId;
	}

	@Override
	public String toString() {
		return "PageModel [totalItem=" + totalItem + ", totalPages="
				+ totalPages + ", pageSize=" + pageSize + ", currPageNo="
				+ currPageNo + ", prePageNo=" + prePageNo + ", nextPageNo="
				+ nextPageNo + ", firstPageNo=" + firstPageNo + ", endPageNo="
				+ endPageNo + ", startItemNo=" + startItemNo + ", endItemNo="
				+ endItemNo + ", isFirstPageNo=" + isFirstPageNo
				+ ", isEndPageNo=" + isEndPageNo + ", isHasPrePageNo="
				+ isHasPrePageNo + ", isHasNextPageNo=" + isHasNextPageNo
				+ ", lastPageEndId=" + lastPageEndId + "]";
	}
	
	/**
	 * 打印当前分页实例的所有信息
	 */
	public void printlnInfo(){
		System.out.println("########################################");
		Class<? extends PageModel> clazz = this.getClass();
		Method[] methods = clazz.getMethods();
		for(Method m:methods){
			String name = m.getName();
			if(name.startsWith("get")){
//			if(name.startsWith("get")||name.startsWith("is")){
				try {
					Object invoke = m.invoke(this);
					System.out.println(name.replaceFirst("get", "")+"==>>"+String.valueOf(invoke));
				} catch (IllegalArgumentException e) {
					e.printStackTrace();
				} catch (IllegalAccessException e) {
					e.printStackTrace();
				} catch (InvocationTargetException e) {
					e.printStackTrace();
				}
			}
		}
		System.out.println("########################################");
	}
}
