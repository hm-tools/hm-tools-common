package top.hmtools.model;

import java.io.Serializable;
import java.util.List;

import top.hmtools.enums.EDataFormat;

/**
 * 描述controller信息的实体类
 * @author HyboJ
 *
 */
public class ControllerInfoModel implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3612570027064369494L;

	/**
	 * controller名称
	 */
	private String name;
	
	/**
	 * 用途
	 */
	private String usedFor;
	
	/**
	 * 输入参数信息
	 */
	private List<ParamInfoModel> paramInfoModels;
	
	/**
	 * 输入参数数据格式
	 */
	private EDataFormat inputDataFormat;
	
	/**
	 * 输出数据信息
	 */
	private List<ResultInfoModel> resultInfoModels;
	
	/**
	 * 输出数据格式
	 */
	private EDataFormat outputDataFormat;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getUsedFor() {
		return usedFor;
	}

	public void setUsedFor(String usedFor) {
		this.usedFor = usedFor;
	}

	public List<ParamInfoModel> getParamInfoModels() {
		return paramInfoModels;
	}

	public void setParamInfoModels(List<ParamInfoModel> paramInfoModels) {
		this.paramInfoModels = paramInfoModels;
	}

	public EDataFormat getInputDataFormat() {
		return inputDataFormat;
	}

	public void setInputDataFormat(EDataFormat inputDataFormat) {
		this.inputDataFormat = inputDataFormat;
	}

	public List<ResultInfoModel> getResultInfoModels() {
		return resultInfoModels;
	}

	public void setResultInfoModels(List<ResultInfoModel> resultInfoModels) {
		this.resultInfoModels = resultInfoModels;
	}

	public EDataFormat getOutputDataFormat() {
		return outputDataFormat;
	}

	public void setOutputDataFormat(EDataFormat outputDataFormat) {
		this.outputDataFormat = outputDataFormat;
	}

	@Override
	public String toString() {
		return "ControllerInfoModel [name=" + name + ", usedFor=" + usedFor
				+ ", paramInfoModels=" + paramInfoModels + ", inputDataFormat="
				+ inputDataFormat + ", resultInfoModels=" + resultInfoModels
				+ ", outputDataFormat=" + outputDataFormat + "]";
	}
	
	
}
