package top.hmtools.model;

import java.io.Serializable;

/**
 * 参数描述信息实体对象类
 * @author Jianghaibo
 *
 */
public class ParamInfoModel implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -2024734548460607506L;

    /**
     * 参数名称
     */
    private String name;
    
    /**
     * 参数数据类型
     */
    private String dataType;
    
    /**
     * 参数最小数据长度
     */
    private String dataMinLength;
    
    /**
     * 参数最大数据长度
     */
    private String dataMaxLength;
    
    /**
     * 备注
     */
    private String comment;
    
    /**
     * 是否必须
     */
    private boolean isRequired;
    
    /**
     * 缺省值
     */
    private String defaultValue;

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the dataType
     */
    public String getDataType() {
        return dataType;
    }

    /**
     * @param dataType the dataType to set
     */
    public void setDataType(String dataType) {
        this.dataType = dataType;
    }

    /**
     * @return the dataMinLength
     */
    public String getDataMinLength() {
        return dataMinLength;
    }

    /**
     * @param dataMinLength the dataMinLength to set
     */
    public void setDataMinLength(String dataMinLength) {
        this.dataMinLength = dataMinLength;
    }

    /**
     * @return the dataMaxLength
     */
    public String getDataMaxLength() {
        return dataMaxLength;
    }

    /**
     * @param dataMaxLength the dataMaxLength to set
     */
    public void setDataMaxLength(String dataMaxLength) {
        this.dataMaxLength = dataMaxLength;
    }

    /**
     * @return the comment
     */
    public String getComment() {
        return comment;
    }

    /**
     * @param comment the comment to set
     */
    public void setComment(String comment) {
        this.comment = comment;
    }

	public boolean isRequired() {
		return isRequired;
	}

	public void setRequired(boolean isRequired) {
		this.isRequired = isRequired;
	}

	public String getDefaultValue() {
		return defaultValue;
	}

	public void setDefaultValue(String defaultValue) {
		this.defaultValue = defaultValue;
	}

	@Override
	public String toString() {
		return "ParamInfoModel [name=" + name + ", dataType=" + dataType
				+ ", dataMinLength=" + dataMinLength + ", dataMaxLength="
				+ dataMaxLength + ", comment=" + comment + ", isRequired="
				+ isRequired + ", defaultValue=" + defaultValue + "]";
	}
    
    
}
