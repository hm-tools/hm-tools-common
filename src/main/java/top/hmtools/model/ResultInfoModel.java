package top.hmtools.model;

import java.io.Serializable;

public class ResultInfoModel implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 505684733902808627L;

	/**
	 * 参数名称
	 */
	private String name;

	/**
	 * 参数数据类型
	 */
	private String dataType;

	/**
	 * 参数最小数据长度
	 */
	private String dataMinLength;

	/**
	 * 参数最大数据长度
	 */
	private String dataMaxLength;

	/**
	 * 备注
	 */
	private String comment;
	
	 /**
     * 是否必须
     */
    private boolean isRequired;
    
    /**
     * 缺省值
     */
    private String defaultValue;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDataType() {
		return dataType;
	}

	public void setDataType(String dataType) {
		this.dataType = dataType;
	}

	public String getDataMinLength() {
		return dataMinLength;
	}

	public void setDataMinLength(String dataMinLength) {
		this.dataMinLength = dataMinLength;
	}

	public String getDataMaxLength() {
		return dataMaxLength;
	}

	public void setDataMaxLength(String dataMaxLength) {
		this.dataMaxLength = dataMaxLength;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public boolean isRequired() {
		return isRequired;
	}

	public void setRequired(boolean isRequired) {
		this.isRequired = isRequired;
	}

	public String getDefaultValue() {
		return defaultValue;
	}

	public void setDefaultValue(String defaultValue) {
		this.defaultValue = defaultValue;
	}

	@Override
	public String toString() {
		return "ResultInfoModel [name=" + name + ", dataType=" + dataType
				+ ", dataMinLength=" + dataMinLength + ", dataMaxLength="
				+ dataMaxLength + ", comment=" + comment + ", isRequired="
				+ isRequired + ", defaultValue=" + defaultValue + "]";
	}
	
	
}
