package top.hmtools.annotation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import top.hmtools.enums.EDataFormat;

/**
 * @Target(ElementType.TYPE)   //接口、类、枚举、注解
@Target(ElementType.FIELD) //字段、枚举的常量
@Target(ElementType.METHOD) //方法
@Target(ElementType.PARAMETER) //方法参数
@Target(ElementType.CONSTRUCTOR)  //构造函数
@Target(ElementType.LOCAL_VARIABLE)//局部变量
@Target(ElementType.ANNOTATION_TYPE)//注解
@Target(ElementType.PACKAGE) ///包 
 */

/**
 * 描述controller信息的注解
 * @author HyboJ
 * 创建日期：2017-1-17下午4:08:06
 */

@Target(value = { ElementType.METHOD,ElementType.TYPE })
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface ControllerInfo{
    
    /**
     * 名称
    * 方法说明：                    name
     */
    String name() default "";
    
    /**
     * 用途
     * @return
     */
    String usedFor() default "";
    
    /**
     * 请求参数参数信息列表
    * @return
     */
    ParamInfo[] paramInfos() default @ParamInfo;
    
    /**
     * 输入数据格式
    * 输入参数说明：           @return
     */
    EDataFormat inputDataFormat() default EDataFormat.DEFAULT;
    
    /**
     * 输出数据格式
    * 输入参数说明：           @return
     */
    EDataFormat outputDataFormat() default EDataFormat.DEFAULT;
    
    /**
     * 返回的结果信息列表
    * @return
     */
    ResultInfo[] ResultInfos() default @ResultInfo;
}


