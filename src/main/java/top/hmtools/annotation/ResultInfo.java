package top.hmtools.annotation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import top.hmtools.enums.EDataType;

/**
 * @Target(ElementType.TYPE)   //接口、类、枚举、注解
@Target(ElementType.FIELD) //字段、枚举的常量
@Target(ElementType.METHOD) //方法
@Target(ElementType.PARAMETER) //方法参数
@Target(ElementType.CONSTRUCTOR)  //构造函数
@Target(ElementType.LOCAL_VARIABLE)//局部变量
@Target(ElementType.ANNOTATION_TYPE)//注解
@Target(ElementType.PACKAGE) ///包 
 */

/**
 * 描述controller返回结果信息的注解
 * @author Jianghaibo
 *
 */
@Target(value = {ElementType.ANNOTATION_TYPE })
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface ResultInfo {
    
    /**
     *结果名称 
    *  @return
     */
    String resultName() default "";

    /**
     * 最小数据长度
     *  <br>“-1”表示不限
    * 输入参数说明：           @return
     */
    int minLength() default -1;
    
    /**
     * 最大数据长度
     *  <br>“-1”表示不限
    * 输入参数说明：           @return
     */
    int maxLength() default -1;
    
    /**
     * 备注说明
    * 输入参数说明：           @return
     */
    String comment() default "";
    
    /**
     * 数据类型
    * 输入参数说明：           @return
     */
    EDataType dataType() default EDataType.Default;
    
    /**
     * 是否必须
    * 输入参数说明：           @return
     */
    boolean isRequired() default false;
    
    /**
     * 缺省值
    * 输入参数说明：           @return
     */
    String defaultValue() default "";
}
