package top.hmtools.io;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.channels.FileChannel;
import java.nio.charset.Charset;
import java.nio.charset.CharsetDecoder;
import java.util.ArrayList;
import java.util.List;

import top.hmtools.base.CharsetTools;
import top.hmtools.base.StringTools;

/**
 * 通过NIO读写文件
 * @author Hybomyth
 * 创建日期：2016-12-11下午8:56:27
 */
public class NioFileTools {
	
	/**
	 * nio基于缓冲区读取指定路径的一个文件
	 * <br>可用于读取大文件，在硬件配置足够的情况下
	 * @param file
	 * @return
	 * @throws IOException 
	 */
	public static byte[] readOneFileToByte(File file) throws IOException{
		return readOneFileToByte(file.getAbsolutePath());
	}

	/**
	 * nio基于缓冲区读取指定路径的一个文件
	 * <br>可用于读取大文件，在硬件配置足够的情况下
	 * @param pathname	文件所在路径
	 * @return	字节数组
	 * @throws IOException 
	 */
	public static byte[] readOneFileToByte(String pathname) throws IOException{
		return readOneFileToByte(pathname, null);
	}

	/**
	 * nio基于缓冲区读取指定路径的一个文件
	 * <br>可用于读取大文件，在硬件配置足够的情况下
	 * @param pathname
	 * @param streamProgress	文件数据流读取进度条
	 * @return
	 * @throws IOException
	 */
	public static byte[] readOneFileToByte(String pathname,IStreamProgress streamProgress) throws IOException{
		FileInputStream fis = new FileInputStream(pathname);
		byte[] result = NioTools.readToByte(fis, streamProgress);
		return result;
	}
	
	/**
	 * 读取一个文件内容，并转换为字符串格式
	 * @param pathname
	 * @return
	 * @throws IOException 
	 */
	public static String readOneFileToString(String pathname,String charsetName) throws IOException{
		FileInputStream fis = new FileInputStream(pathname);
		Charset charset = CharsetTools.toCharset(charsetName);
		String result = NioTools.readToString(fis, charset);
		return result;
	}
	
	/**
	 * 读取多个文件 到 列表
	 * @param streamProgress
	 * @param pathnames
	 * @return
	 * @throws IOException
	 */
	public static List<byte[]> readSomeFile(IStreamProgress streamProgress,String... pathnames) throws IOException{
		List<byte[]> result = new ArrayList<byte[]>();
		long count = 0;
		
		if(null != streamProgress){
			streamProgress.start();
		}
		
		for(String path:pathnames){
			byte[] oneFileByte = readOneFileToByte(path);
			result.add(oneFileByte);
			count += oneFileByte.length;
			if(null != streamProgress){
				streamProgress.progress(count);
			}
		}
		
		if(null != streamProgress){
			streamProgress.finish();
		}
		return result;
	}
	
	/**
	 * 读取多个文件 到 列表
	 * @param pathnames
	 * @return
	 * @throws IOException
	 */
	public static List<byte[]> readSomeFile(String... pathnames) throws IOException {
		return readSomeFile(null, pathnames);
	}
	
	/**
	 * 读取多个文件 到 字符串列表
	 * @param pathnames
	 * @param charsetName
	 * @return
	 * @throws IOException 
	 */
	public static List<String> readSomeFileToStrs(String charsetName,String... pathnames) throws IOException{
		return readSomeFileToStrs(null, charsetName, pathnames);
	}
	
	/**
	 * 读取多个文件 到 字符串列表
	 * @param pathnames
	 * @param charsetName
	 * @return
	 * @throws IOException 
	 */
	public static List<String> readSomeFileToStrs(IStreamProgress streamProgress,String charsetName,String... pathnames) throws IOException{
		List<String> result = new ArrayList<String>();
		long count = 0;
		
		if(null != streamProgress){
			streamProgress.start();
		}
		
		for(String path:pathnames){
			String oneFileStr = readOneFileToString(path, charsetName);
			result.add(oneFileStr);
			count += (long)oneFileStr.length();
			if(null != streamProgress){
				streamProgress.progress(count);
			}
		}
		
		if(null != streamProgress){
			streamProgress.finish();
		}
		return result;
	}
	
	/**
	 * 读取多个文件，并合并成一条字符串
	 * @param pathnames	读取的源文件集合
	 * @param charsetName	字符编码名称
	 * @param linkChar	多文件合并的连接符号，缺省为“”空字符串
	 * @return
	 * @throws IOException 
	 */
	public static String readSomeFileToStr(String charsetName,char linkChar,String... pathnames) throws IOException{
		List<String> someFileStrs = readSomeFileToStrs(charsetName, pathnames);
		StringBuffer sb_result  = new StringBuffer();
		for(String strTmp:someFileStrs){
			sb_result=sb_result.append(strTmp);
			sb_result=sb_result.append(linkChar);
		}
		String linkStr = String.valueOf(linkChar);
		int lastIndexOfLinkStr = sb_result.lastIndexOf(linkStr);
		String result = sb_result.substring(0, lastIndexOfLinkStr);
		return result;
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}
