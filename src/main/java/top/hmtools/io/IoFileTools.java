package top.hmtools.io;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * 通过 io 读取文件
 * @author Hybomyth
 * 创建日期：2016-12-11下午8:56:04
 */
public class IoFileTools {
	
	/**
	 * 获取当前文件夹或者当前文件所在文件夹下所有的文件列表信息
	 * @param pathname
	 * @return
	 */
	public static File[] getDirFileList(String pathname){
		File[] result = null;
		try {
			File file = new File(pathname);
			if(!file.exists()){
				return result;
			}
			if(file.isDirectory()){
				result = file.listFiles();
			}else{
				String path = file.getParent();
				File fileTmp = new File(path);
				result = fileTmp.listFiles();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}finally{
			
		}
		return result;
	}
	
	/**
	 * 获取当前文件所在目录（或者当前目录）下所有文件夹列表
	 * @param pathname
	 * @return
	 */
	public static File[] getDirList(String pathname){
		File[] result = new File[0];
		try {
			File[] dirFileList = getDirFileList(pathname);
			if(dirFileList.length <1){
				return result;
			}
			List<File> filesTmp = new ArrayList<File>();
			for(File f:dirFileList){
				if(f.isDirectory()){
					filesTmp.add(f);
				}
			}
			result = filesTmp.toArray(new File[0]);
		} catch (Exception e) {
			e.printStackTrace();
		}finally{
			
		}
		return result;
	}
	
	/**
	 * 获取当前文件所在目录（或者当前目录）下所有文件（不包括文件夹）列表
	 * @param pathname
	 * @return
	 */
	public static File[] getFileList(String pathname){
		File[] result = new File[0];
		try {
			File[] dirFileList = getDirFileList(pathname);
			if(dirFileList.length <1){
				return result;
			}
			List<File> filesTmp = new ArrayList<File>();
			for(File f:dirFileList){
				if(!f.isDirectory()){
					filesTmp.add(f);
				}
			}
			result = filesTmp.toArray(new File[0]);
		} catch (Exception e) {
			e.printStackTrace();
		}finally{
			
		}
		return result;
	}
	//用于遍历获取路径时的结果集
	private static File[] result = new File[0];
	
	/**
	 * 获取指定路径下的所有文件夹及文件列表数据集合（包括其子目录）
	 * @param pathname
	 * @param level
	 */
	private  static void getAllDirFileList(String pathname,Integer level){
		try {
			//初始化 层级 level
			if(level == null){
				level = 0;
			}
			if(level == 0){
				result=new File[0];
			}
			
			//获取当前目录下所有文件夹及文件信息
			File[] currDirFiles = getDirFileList(pathname);
			
			//将结果追加到 结果集
			File[] tmp = new File[result.length+currDirFiles.length];
			System.arraycopy(result, 0, tmp, 0, result.length);
			System.arraycopy(currDirFiles, 0, tmp, result.length, currDirFiles.length);
			result = tmp;
			
			//遍历 并 递归，获取下一级文件夹的所有文件夹及文件信息
			for(File file:currDirFiles){
				if(file.isDirectory()){
					level++;
					getAllDirFileList(file.getAbsolutePath(),level);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	 *  获取指定路径下的所有文件夹及文件列表数据集合（包括其子目录）
	 * @param pathname
	 * @return
	 */
	public static File[] getAllDirFileList(String pathname){
		getAllDirFileList(pathname, 0);
		return result;
	}
	
	/**
	 * 过滤出所有 文件夹 
	 * @param files
	 * @return
	 */
	public static File[] getDirs(File[] files){
		File [] result = new File[0];
		try {
			List<File> tmp = new ArrayList<File>();
			for(File file:files){
				if(file.isDirectory()){
					tmp.add(file);
				}
			}
			result = tmp.toArray(new File[0]);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}
	
	/**
	 * 过滤出所有 文件
	 * @param files
	 * @return
	 */
	public static File[] getFiles(File[] files){
		File [] result = new File[0];
		try {
			List<File> tmp = new ArrayList<File>();
			for(File file:files){
				if(!file.isDirectory()){
					tmp.add(file);
				}
			}
			result = tmp.toArray(new File[0]);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}
	
	/**
	 * 获取 指定 目录下及其子文件夹下的所有文件
	 * @param pathname
	 * @return
	 */
	public static File[] getAllFiles(String pathname){
		File[] allDirFileList = getAllDirFileList(pathname);
		return getFiles(allDirFileList);
	}
	
	public static File[] getFilterFilesBySuffixes(File[] files ,String... suffixes){
		File[] result = new File[0];
		if(suffixes == null || suffixes.length<1){
			return files;
		}
		try {
			File[] files_tmp = getFiles(files);
			List<File> files_list	= new ArrayList<File>();
			for(File file:files_tmp){
				String fileName = file.getName();
				for(String suffix:suffixes){
					boolean endsWith = fileName.toUpperCase().trim().endsWith(suffix.toUpperCase().trim());
					if(endsWith){
						files_list.add(file);
					}
				}
			}
			result = files_list.toArray(new File[0]);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}
	
	/**
	 * 根据多个文件名后缀 过滤 文件集合
	 * @param files
	 * @param suffixes
	 * @return
	 */
	public static File[] getFilterFilesBySuffixes(File[] files ,List<String> suffixes){
		String[] tmpArr = new String[suffixes.size()];
		String[] result = suffixes.toArray(tmpArr);
		return getFilterFilesBySuffixes(files,result);
	}
	
	/**
	 * 根据一条文件名后缀 过滤 文件集合
	 * @param files
	 * @param suffix
	 * @return
	 */
	public static File[] getFilterFilesBySuffix(File[] files,String suffix){
		List<String> suffixes = new ArrayList<String>();
		suffixes.add(suffix);
		return getFilterFilesBySuffixes(files, suffixes);
	}
	
	/**
	 * 获取指定目录文件夹下包括子目录所有以指定后缀数据集合的文件记录集合
	 * @param pathname
	 * @param suffixes
	 * @return
	 */
	public static File[] getAllFilterFilesBySuffixes(String pathname,List<String> suffixes){
		File[] allFiles = getAllFiles(pathname);
		return getFilterFilesBySuffixes(allFiles, suffixes);
	}
	
	/**
	 * 获取指定文件夹所在磁盘分区的总容量
	 * <br>单位 Byte
	 * @param path
	 * @return
	 */
	public static long getDiskPartitionTotalSpaceByte(File path){
		long result = 0;
		try {
			result = path.getTotalSpace();
		} catch (Exception e) {
			e.printStackTrace();
		}finally{
			
		}
		return result;
	}
	
	/**
	 * 获取指定文件夹所在磁盘分区的总容量
	 * <br>单位 KByte
	 * @param path
	 * @return
	 */
	public static long getDiskPartitionTotalSpaceKB(File path){
		return getDiskPartitionTotalSpaceByte(path)/1024;
	}
	
	/**
	 * 获取指定文件夹所在磁盘分区的总容量
	 * <br>单位 MByte
	 * @param path
	 * @return
	 */
	public static long getDiskPartitionTotalSpaceMB(File path){
		return getDiskPartitionTotalSpaceKB(path)/1024;
	}
	
	/**
	 * 获取指定文件夹所在磁盘分区的总容量
	 * <br>单位 GByte
	 * @param path
	 * @return
	 */
	public static long getDiskPartitionTotalSpaceGB(File path){
		return getDiskPartitionTotalSpaceMB(path)/1024;
	}
	
	/**
	 * 获取指定文件夹所在磁盘分区的总容量
	 * <br>单位 TByte
	 * @param path
	 * @return
	 */
	public static long getDiskPartitionTotalSpaceTB(File path){
		return getDiskPartitionTotalSpaceGB(path)/1024;
	}
	
	/**
	 * 获取指定文件夹所在磁盘分区的空闲容量
	 * <br>单位：Byte
	 * @param path
	 * @return
	 */
	public static long getDiskPartitionFreeSpaceByte(File path){
		long result = 0;
		try {
			result = path.getFreeSpace();
		} catch (Exception e) {
			e.printStackTrace();
		}finally{
			
		}
		return result;
	}
	
	/**
	 * 获取指定文件夹所在磁盘分区的空闲容量
	 * <br>单位：KB
	 * @param path
	 * @return
	 */
	public static long getDiskPartitionFreeSpaceKB(File path){
		return getDiskPartitionFreeSpaceByte(path)/1024;
	}
	
	/**
	 * 获取指定文件夹所在磁盘分区的空闲容量
	 * <br>单位：MB
	 * @param path
	 * @return
	 */
	public static long getDiskPartitionFreeSpaceMB(File path){
		return getDiskPartitionFreeSpaceKB(path)/1024;
	}
	
	/**
	 * 获取指定文件夹所在磁盘分区的空闲容量
	 * <br>单位：GB
	 * @param path
	 * @return
	 */
	public static long getDiskPartitionFreeSpaceGB(File path){
		return getDiskPartitionFreeSpaceMB(path)/1024;
	}
	
	/**
	 * 获取指定文件夹所在磁盘分区的可用容量
	 * <br>单位：Byte
	 * @param path
	 * @return
	 */
	public static long getDiskPartitionUsableSpaceByte(File path){
		long result = 0;
		try {
			result = path.getUsableSpace();
		} catch (Exception e) {
			e.printStackTrace();
		}finally{
			
		}
		return result;
	}
	
	/**
	 * 获取指定文件夹所在磁盘分区的可用容量
	 * <br>单位：KB
	 * @param path
	 * @return
	 */
	public static long getDiskPartitionUsableSpaceKB(File path){
		return getDiskPartitionUsableSpaceByte(path)/1024;
	}
	
	/**
	 * 获取指定文件夹所在磁盘分区的可用容量
	 * <br>单位：MB
	 * @param path
	 * @return
	 */
	public static long getDiskPartitionUsableSpaceMB(File path){
		return getDiskPartitionUsableSpaceKB(path)/1024;
	}
	
	/**
	 * 获取指定文件夹所在磁盘分区的可用容量
	 * <br>单位：GB
	 * @param path
	 * @return
	 */
	public static long getDiskPartitionUsableSpaceGB(File path){
		return getDiskPartitionUsableSpaceMB(path)/1024;
	}

}
