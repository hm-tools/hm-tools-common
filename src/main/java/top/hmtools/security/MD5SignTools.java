package top.hmtools.security;

import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import top.hmtools.collection.MapTools;

/**
 * 基于MD5的签名工具
 * @author Hybomyth
 * 创建日期：2016-12-12上午9:15:54
 */
public class MD5SignTools {

	/**
	 * 对map中数据按字典排序后，并按指定连接符号连接，获取MD5签名
	 * @param params	需要进行签名的源数据
	 * @param filterKey	需要进行过滤掉，不参与签名的key列表
	 * @param k_v	键值对之间的连接符号，缺省：=
	 * @param i_i	成员数据之间的连接符，默认：&
	 * @return	签名结果
	 */
	public static String getLowMd5Sign(Map<String,String> params,List<String> filterKey,String k_v,String i_i){
		String result = null;
		try {
			//过滤map
			Map<String, String> filterMap = MapTools.getFilterMap(params, filterKey);
			
			//获取treemap
			TreeMap<String,String> treeMap = MapTools.getTreeMap(filterMap);
			
			//获取指定连接方式的字符串,并字典排序
			String orderDictStr = MapTools.getOrderDictStr(treeMap, k_v, i_i);
			
			//获取MD5签名
			result = MD5Tools.getLowMD5Str(orderDictStr);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}
}
