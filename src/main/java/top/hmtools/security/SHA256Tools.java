package top.hmtools.security;

import java.io.File;
import java.io.InputStream;
import java.nio.charset.Charset;

/**
 * SHA-256算法工具类
 * @author Jianghaibo
 *
 */
public class SHA256Tools extends SecurityTools{
    
    private final static ESecurityName securityType = ESecurityName.SHA256;

    /**
     * 获取输入流的加密结果
     *  <br>20180315 通过了与Apache Commons codec 的计算结果比对
    * <br>方法说明：                    getLowStr
    * <br>输入参数说明：           
    * <br>@param inputStream
    * <br>@return
    * <br>输出参数说明：
    * <br>String           
    *
     */
    public  static String getLowStr(InputStream inputStream) {
        return SecurityTools.getLowStr(inputStream, securityType);
    }
    
    /**
     * 获取文件的加密结果
     *  <br>20180315 通过了与Apache Commons codec 的计算结果比对
    * <br>方法说明：                    getLowStr
    * <br>输入参数说明：           
    * <br>@param file
    * <br>@return
    * <br>输出参数说明：
    * <br>String           
    *
     */
    public  static String getLowStr(File file) {
        return SecurityTools.getLowStr(file, securityType);
    }
    
    /**
     * 获取字符串的加密结果
     *  <br>20180315 通过了与Apache Commons codec 的计算结果比对
    * <br>方法说明：                    getLowStr
    * <br>输入参数说明：           
    * <br>@param str
    * <br>@return
    * <br>输出参数说明：
    * <br>String           
    *
     */
    public  static String getLowStr(String str) {
        return SecurityTools.getLowStr(str, securityType);
    }
    
    /**
     * 获取字节数组的加密结果
     *  <br>20180315 通过了与Apache Commons codec 的计算结果比对
    * <br>方法说明：                    getLowStr
    * <br>输入参数说明：           
    * <br>@param bytes
    * <br>@return
    * <br>输出参数说明：
    * <br>String           
    *
     */
    public  static String getLowStr(byte[] bytes) {
        return SecurityTools.getLowStr(bytes, securityType);
    }
    
    /**
     * 获取字符串的加密结果
     *  <br>20180315 通过了与Apache Commons codec 的计算结果比对
    * <br>方法说明：                    getLowStr
    * <br>输入参数说明：           
    * <br>@param str
    * <br>@param charset
    * <br>@return
    * <br>输出参数说明：
    * <br>String           
    *
     */
    public  static String getLowStr(String str,Charset charset) {
        return SecurityTools.getLowStr(str, charset,securityType);
    }
}
