package top.hmtools.system;

import top.hmtools.base.StringTools;

/**
 * 获取当前系统环境信息的工具类
 * 
 * @author HyboJ
 * 
 */
public class SystemInfoTools {

	/**
	 * 获取运行时名称
	 * 
	 * @return
	 */
	public static String getRuntimeName() {
		return System.getProperty("java.runtime.name");
	}

	/**
	 * 获取用户当前运行Java程序所在的路径
	 * 
	 * @return
	 */
	public static String getUserDir() {
		return System.getProperty("user.dir");
	}

	/**
	 * 获取用户当前运行Java程序所在的路径
	 * 
	 * @return
	 */
	public static String getCurrentProjectDir() {
		return getUserDir();
	}

	/**
	 * 获取当前操作系统名称
	 * 
	 * @return
	 */
	public static String getOSName() {
		return System.getProperty("os.name");
	}

	/**
	 * 获取当前运行环境的jdk版本号（字符串数据类型结果）
	 * 
	 * @return
	 */
	public static String getJDKVersionStr() {
		return System.getProperty("java.version");
	}
	
	/**
	 * 取得当前Java impl.的版本的<code>float</code>值。
	 * 
	 * @return Java版本的<code>float</code>值或<code>0</code>
	 */
	public static float getJDKVersionFloat() {
		String jdkVersionStr = getJDKVersionStr();
		if (jdkVersionStr == null) {
			return 0f;
		}

		String str = jdkVersionStr.substring(0, 3);

		if (jdkVersionStr.length() >= 5) {
			str = str + jdkVersionStr.substring(4, 5);
		}

		return Float.parseFloat(str);
	}
	
	/**
	 * 取得当前Java impl.的版本的<code>int</code>值。
	 * 
	 * @return Java版本的<code>int</code>值或<code>0</code>
	 */
	public static int getJDKVersionInt() {
		String jdkVersionStr = getJDKVersionStr();
		if (jdkVersionStr == null) {
			return 0;
		}

		String str = jdkVersionStr.substring(0, 1);

		str = str + jdkVersionStr.substring(2, 3);

		if (jdkVersionStr.length() >= 5) {
			str = str + jdkVersionStr.substring(4, 5);
		} else {
			str = str + "0";
		}

		return Integer.parseInt(str);
	}
	
	/**
	 * 判断当前Java的版本。
	 * 
	 * <p>
	 * 如果不能取得系统属性<code>java.version</code>（因为Java安全限制），则总是返回 <code>false</code>
	 * 
	 * 
	 * @return 如果当前Java版本为1.1，则返回<code>true</code>
	 */
	public static boolean isJava11() {
		return getJavaVersionMatches("1.1");
	}

	/**
	 * 判断当前Java的版本。
	 * 
	 * <p>
	 * 如果不能取得系统属性<code>java.version</code>（因为Java安全限制），则总是返回 <code>false</code>
	 * 
	 * 
	 * @return 如果当前Java版本为1.2，则返回<code>true</code>
	 */
	public static boolean isJava12() {
		return getJavaVersionMatches("1.2");
	}

	/**
	 * 判断当前Java的版本。
	 * 
	 * <p>
	 * 如果不能取得系统属性<code>java.version</code>（因为Java安全限制），则总是返回 <code>false</code>
	 * 
	 * 
	 * @return 如果当前Java版本为1.3，则返回<code>true</code>
	 */
	public static boolean isJava13() {
		return getJavaVersionMatches("1.3");
	}

	/**
	 * 判断当前Java的版本。
	 * 
	 * <p>
	 * 如果不能取得系统属性<code>java.version</code>（因为Java安全限制），则总是返回 <code>false</code>
	 * 
	 * 
	 * @return 如果当前Java版本为1.4，则返回<code>true</code>
	 */
	public static boolean isJava14() {
		return getJavaVersionMatches("1.4");
	}

	/**
	 * 判断当前Java的版本。
	 * 
	 * <p>
	 * 如果不能取得系统属性<code>java.version</code>（因为Java安全限制），则总是返回 <code>false</code>
	 * 
	 * 
	 * @return 如果当前Java版本为1.5，则返回<code>true</code>
	 */
	public static boolean isJava15() {
		return getJavaVersionMatches("1.5");
	}

	/**
	 * 判断当前Java的版本。
	 * 
	 * <p>
	 * 如果不能取得系统属性<code>java.version</code>（因为Java安全限制），则总是返回 <code>false</code>
	 * 
	 * 
	 * @return 如果当前Java版本为1.6，则返回<code>true</code>
	 */
	public static boolean isJava16() {
		return getJavaVersionMatches("1.6");
	}

	/**
	 * 判断当前Java的版本。
	 * 
	 * <p>
	 * 如果不能取得系统属性<code>java.version</code>（因为Java安全限制），则总是返回 <code>false</code>
	 * 
	 * 
	 * @return 如果当前Java版本为1.7，则返回<code>true</code>
	 */
	public static boolean isJava17() {
		return getJavaVersionMatches("1.7");
	}

	/**
	 * 判断当前Java的版本。
	 *
	 * <p>
	 * 如果不能取得系统属性<code>java.version</code>（因为Java安全限制），则总是返回 <code>false</code>
	 * 
	 *
	 * @return 如果当前Java版本为1.8，则返回<code>true</code>
	 */
	public static boolean isJava18() {
		return getJavaVersionMatches("1.8");
	}

	/**
	 * 匹配当前Java的版本。
	 * 
	 * @param versionPrefix Java版本前缀
	 * 
	 * @return 如果版本匹配，则返回<code>true</code>
	 */
	public static boolean getJavaVersionMatches(String versionPrefix) {
		String jdkVersionStr = getJDKVersionStr();
		if (jdkVersionStr == null) {
			return false;
		}

		return jdkVersionStr.startsWith(versionPrefix);
	}
	
	/**
	 * 判定当前Java的版本是否大于等于指定的版本号。
	 * 
	 * <p>
	 * 例如：
	 * 
	 * 
	 * <ul>
	 * <li>测试JDK 1.2：<code>isJavaVersionAtLeast(1.2f)</code></li>
	 * <li>测试JDK 1.2.1：<code>isJavaVersionAtLeast(1.31f)</code></li>
	 * </ul>
	 * 
	 * 
	 * @param requiredVersion 需要的版本
	 * 
	 * @return 如果当前Java版本大于或等于指定的版本，则返回<code>true</code>
	 */
	public static boolean isJavaVersionAtLeast(float requiredVersion) {
		return getJDKVersionFloat() >= requiredVersion;
	}
	
	/**
	 * 判定当前Java的版本是否大于等于指定的版本号。
	 * 
	 * <p>
	 * 例如：
	 * 
	 * 
	 * <ul>
	 * <li>测试JDK 1.2：<code>isJavaVersionAtLeast(120)</code></li>
	 * <li>测试JDK 1.2.1：<code>isJavaVersionAtLeast(131)</code></li>
	 * </ul>
	 * 
	 * 
	 * @param requiredVersion 需要的版本
	 * 
	 * @return 如果当前Java版本大于或等于指定的版本，则返回<code>true</code>
	 */
	public static boolean isJavaVersionAtLeast(int requiredVersion) {
		return getJDKVersionInt() >= requiredVersion;
	}
	
	/**
	 * JVM is 32M <code>or</code> 64M
	 * 
	 * @return 32 <code>or</code> 64
	 */
	public static String getSunArchDataModel() {
		return System.getProperty("sun.arch.data.model");
	}
	
	/**
	 * 获取sun.boot.class.path
	 * @return
	 */
	public static String getSunBootClassPath() {
		return System.getProperty("sun.boot.class.path");
	}
	
	/**
	 * 取得当前JRE的名称（取自系统属性：<code>java.runtime.name</code>）。
	 * 
	 * <p>
	 * 例如Sun JDK 1.4.2： <code>"Java(TM) 2 Runtime Environment, Standard Edition"</code>
	 * </p>
	 * 
	 * @return 属性值，如果不能取得（因为Java安全限制）或值不存在，则返回<code>null</code>。
	 * 
	 * @since Java 1.3
	 */
	public static String getJREName() {
		return System.getProperty("java.runtime.name");
	}
	
	/**
	 * 取得当前JRE的版本（取自系统属性：<code>java.runtime.version</code>）。
	 * 
	 * <p>
	 * 例如Sun JDK 1.4.2：<code>"1.4.2-b28"</code>
	 * </p>
	 * 
	 * @return 属性值，如果不能取得（因为Java安全限制）或值不存在，则返回<code>null</code>。
	 * 
	 * @since Java 1.3
	 */
	public static String getJREVersion() {
		return System.getProperty("java.runtime.version");
	}
	
	/**
	 * 取得当前JRE的安装目录（取自系统属性：<code>java.home</code>）。
	 * 
	 * <p>
	 * 例如Sun JDK 1.4.2：<code>"/opt/jdk1.4.2/jre"</code>
	 * </p>
	 * 
	 * @return 属性值，如果不能取得（因为Java安全限制）或值不存在，则返回<code>null</code>。
	 * 
	 * @since Java 1.1
	 */
	public static String getJREHomeDir() {
		return System.getProperty("java.home");
	}
	
	/**
	 * 取得当前JRE的扩展目录列表（取自系统属性：<code>java.ext.dirs</code>）。
	 * 
	 * <p>
	 * 例如Sun JDK 1.4.2：<code>"/opt/jdk1.4.2/jre/lib/ext:..."</code>
	 * </p>
	 * 
	 * @return 属性值，如果不能取得（因为Java安全限制）或值不存在，则返回<code>null</code>。
	 * 
	 * @since Java 1.3
	 */
	public static String getJREExtDirs() {
		return System.getProperty("java.ext.dirs");
	}
	
	/**
	 * 取得当前JRE的endorsed目录列表（取自系统属性：<code>java.endorsed.dirs</code>）。
	 * 
	 * <p>
	 * 例如Sun JDK 1.4.2：<code>"/opt/jdk1.4.2/jre/lib/endorsed:..."</code>
	 * </p>
	 * 
	 * @return 属性值，如果不能取得（因为Java安全限制）或值不存在，则返回<code>null</code>。
	 * 
	 * @since Java 1.4
	 */
	public static String getJREEndorsedDirs() {
		return System.getProperty("java.endorsed.dirs");
	}
	
	/**
	 * 取得当前JRE的系统classpath（取自系统属性：<code>java.class.path</code>）。
	 * 
	 * <p>
	 * 例如：<code>"/home/admin/myclasses:/home/admin/..."</code>
	 * </p>
	 * 
	 * @return 属性值，如果不能取得（因为Java安全限制）或值不存在，则返回<code>null</code>。
	 * 
	 * @since Java 1.1
	 */
	public static String getJREClassPath() {
		return System.getProperty("java.class.path");
	}
	
	/**
	 * 取得当前JRE的系统classpath（取自系统属性：<code>java.class.path</code>）。
	 * 
	 * <p>
	 * 例如：<code>"/home/admin/myclasses:/home/admin/..."</code>
	 * </p>
	 * 
	 * @return 属性值，如果不能取得（因为Java安全限制）或值不存在，则返回<code>null</code>。
	 * 
	 * @since Java 1.1
	 */
	public static String[] getJREClassPathArray() {
		return getJREClassPath().split(System.getProperty("path.separator")) ;
	}
	
	/**
	 * 取得当前JRE的class文件格式的版本（取自系统属性：<code>java.class.version</code>）。
	 * 
	 * <p>
	 * 例如Sun JDK 1.4.2：<code>"48.0"</code>
	 * </p>
	 * 
	 * @return 属性值，如果不能取得（因为Java安全限制）或值不存在，则返回<code>null</code>。
	 * 
	 * @since Java 1.1
	 */
	public static String getJREClassVersion() {
		return System.getProperty("java.class.version");
	}
	
	/**
	 * 取得当前JRE的library搜索路径（取自系统属性：<code>java.library.path</code>）。
	 * 
	 * <p>
	 * 例如Sun JDK 1.4.2：<code>"/opt/jdk1.4.2/bin:..."</code>
	 * </p>
	 * 
	 * @return 属性值，如果不能取得（因为Java安全限制）或值不存在，则返回<code>null</code>。
	 * 
	 */
	public static String getJRELibraryPath() {
		return System.getProperty("java.library.path");
	}
	
	/**
	 * 取得当前JRE的library搜索路径（取自系统属性：<code>java.library.path</code>）。
	 * 
	 * <p>
	 * 例如Sun JDK 1.4.2：<code>"/opt/jdk1.4.2/bin:..."</code>
	 * </p>
	 * 
	 * @return 属性值，如果不能取得（因为Java安全限制）或值不存在，则返回<code>null</code>。
	 * 
	 * 
	 */
	public static String[] getJRELibraryPathArray() {
		return getJRELibraryPath().split(System.getProperty("path.separator"));
	}
	
	/**
	 * 取得当前JRE的URL协议packages列表（取自系统属性：<code>java.library.path</code>）。
	 * 
	 * <p>
	 * 例如Sun JDK 1.4.2：<code>"sun.net.www.protocol|..."</code>
	 * </p>
	 * 
	 * @return 属性值，如果不能取得（因为Java安全限制）或值不存在，则返回<code>null</code>。
	 * 
	 * 
	 */
	public static String getJREProtocolPackages() {
		return System.getProperty("java.protocol.handler.pkgs");
	}
	
	/**
	 * 取得当前JVM impl.的名称（取自系统属性：<code>java.vm.name</code>）。
	 * 
	 * <p>
	 * 例如Sun JDK 1.4.2：<code>"Java HotSpot(TM) Client VM"</code>
	 * </p>
	 * 
	 * @return 属性值，如果不能取得（因为Java安全限制）或值不存在，则返回<code>null</code>。
	 * 
	 */
	public static String getJVMName() {
		return System.getProperty("java.vm.name");
	}
	
	/**
	 * 取得当前JVM impl.的版本（取自系统属性：<code>java.vm.version</code>）。
	 * 
	 * <p>
	 * 例如Sun JDK 1.4.2：<code>"1.4.2-b28"</code>
	 * </p>
	 * 
	 * @return 属性值，如果不能取得（因为Java安全限制）或值不存在，则返回<code>null</code>。
	 * 
	 */
	public static String getJVMVersion() {
		return System.getProperty("java.vm.version");
	}
	
	/**
	 * 取得当前JVM impl.的厂商（取自系统属性：<code>java.vm.vendor</code>）。
	 * 
	 * <p>
	 * 例如Sun JDK 1.4.2：<code>"Sun Microsystems Inc."</code>
	 * </p>
	 * 
	 * @return 属性值，如果不能取得（因为Java安全限制）或值不存在，则返回<code>null</code>。
	 * 
	 */
	public static String getJVMVendor() {
		return System.getProperty("java.vm.vendor");
	}
	
	/**
	 * 取得当前JVM impl.的信息（取自系统属性：<code>java.vm.info</code>）。
	 * 
	 * <p>
	 * 例如Sun JDK 1.4.2：<code>"mixed mode"</code>
	 * </p>
	 * 
	 * @return 属性值，如果不能取得（因为Java安全限制）或值不存在，则返回<code>null</code>。
	 * 
	 */
	public static String getJVMInfo() {
		return System.getProperty("java.vm.info");
	}
	
	
	/**
	 * 取得当前登录用户的名字（取自系统属性：<code>user.name</code>）。
	 * 
	 * <p>
	 * 例如：<code>"admin"</code>
	 * </p>
	 * 
	 * @return 属性值，如果不能取得（因为Java安全限制）或值不存在，则返回<code>null</code>。
	 * 
	 * @since Java 1.1
	 */
	public static String getUserName() {
		return System.getProperty("user.name");
	}
	
	/**
	 * 取得当前登录用户的home目录（取自系统属性：<code>user.home</code>）。
	 * 
	 * <p>
	 * 例如：<code>"/home/admin"</code>
	 * </p>
	 * 
	 * @return 属性值，如果不能取得（因为Java安全限制）或值不存在，则返回<code>null</code>。
	 * 
	 * @since Java 1.1
	 */
	public static String getUserHomeDir() {
		return System.getProperty("user.home");
	}
	
	/**
	 * 取得当前目录（取自系统属性：<code>user.dir</code>）。
	 * 
	 * <p>
	 * 例如：<code>"/home/admin/working"</code>
	 * </p>
	 * 
	 * @return 属性值，如果不能取得（因为Java安全限制）或值不存在，则返回<code>null</code>。
	 * 
	 * @since Java 1.1
	 */
	public static String getUserCurrentDir() {
		return System.getProperty("user.dir");
	}
	
	/**
	 * 取得临时目录（取自系统属性：<code>java.io.tmpdir</code>）。
	 * 
	 * <p>
	 * 例如：<code>"/tmp"</code>
	 * </p>
	 * 
	 * @return 属性值，如果不能取得（因为Java安全限制）或值不存在，则返回<code>null</code>。
	 * 
	 * 
	 */
	public static String getUserTempDir() {
		return System.getProperty("java.io.tmpdir");
	}
	
	/**
	 * 取得当前登录用户的语言设置（取自系统属性：<code>user.language</code>）。
	 * 
	 * <p>
	 * 例如：<code>"zh"</code>、<code>"en"</code>等
	 * </p>
	 * 
	 * @return 属性值，如果不能取得（因为Java安全限制）或值不存在，则返回<code>null</code>。
	 * 
	 */
	public static String getUserLanguage() {
		return System.getProperty("user.language");
	}
	
	/**
	 * 取得当前登录用户的国家或区域设置（取自系统属性：JDK1.4 <code>user.country</code>或JDK1.2 <code>user.region</code>）。
	 * 
	 * <p>
	 * 例如：<code>"CN"</code>、<code>"US"</code>等
	 * </p>
	 * 
	 * @return 属性值，如果不能取得（因为Java安全限制）或值不存在，则返回<code>null</code>。
	 * 
	 */
	public static String getUserCountry() {
		return StringTools.isBlank(System.getProperty("user.country"))?System.getProperty("user.region"):System.getProperty("user.country");
	}
	
	
	/**
	 * 取得当前OS的架构（取自系统属性：<code>os.arch</code>）。
	 * 
	 * <p>
	 * 例如：<code>"x86"</code>
	 * </p>
	 * 
	 * @return 属性值，如果不能取得（因为Java安全限制）或值不存在，则返回<code>null</code>。
	 * 
	 * @since Java 1.1
	 */
	public static String getOSArch() {
		return System.getProperty("os.arch");
	}
	
	
	/**
	 * 取得当前OS的版本（取自系统属性：<code>os.version</code>）。
	 * 
	 * <p>
	 * 例如：<code>"5.1"</code>
	 * </p>
	 * 
	 * @return 属性值，如果不能取得（因为Java安全限制）或值不存在，则返回<code>null</code>。
	 * 
	 * @since Java 1.1
	 */
	public static String getOSVersion() {
		return System.getProperty("os.version");
	}
	
	private static boolean IS_OS_AIX = getOSMatches("AIX");
	private static boolean IS_OS_HP_UX = getOSMatches("HP-UX");
	private static boolean IS_OS_IRIX = getOSMatches("Irix");
	private static boolean IS_OS_LINUX = getOSMatches("Linux") || getOSMatches("LINUX");
	private static boolean IS_OS_MAC = getOSMatches("Mac");
	private static boolean IS_OS_MAC_OSX = getOSMatches("Mac OS X");
	private static boolean IS_OS_OS2 = getOSMatches("OS/2");
	private static boolean IS_OS_SOLARIS = getOSMatches("Solaris");
	private static boolean IS_OS_SUN_OS = getOSMatches("SunOS");
	private static boolean IS_OS_WINDOWS = getOSMatches("Windows");
	private static boolean IS_OS_WINDOWS_2000 = getOSMatches("Windows", "5.0");
	private static boolean IS_OS_WINDOWS_95 = getOSMatches("Windows 9", "4.0");
	private static boolean IS_OS_WINDOWS_98 = getOSMatches("Windows 9", "4.1");
	private static boolean IS_OS_WINDOWS_ME = getOSMatches("Windows", "4.9");
	private static boolean IS_OS_WINDOWS_NT = getOSMatches("Windows NT");
	private static boolean IS_OS_WINDOWS_XP = getOSMatches("Windows", "5.1");
	
	/**
	 * 匹配OS名称。
	 * 
	 * @param osNamePrefix OS名称前缀
	 * @param osVersionPrefix OS版本前缀
	 * 
	 * @return 如果匹配，则返回<code>true</code>
	 */
	private static boolean getOSMatches(String osNamePrefix, String osVersionPrefix) {
		if (StringTools.isBlank(getOSName()) || StringTools.isBlank(getOSVersion())) {
			return false;
		}

		return getOSName().startsWith(osNamePrefix) && getOSVersion().startsWith(osVersionPrefix);
	}
	
	/**
	 * 匹配OS名称。
	 * 
	 * @param osNamePrefix OS名称前缀
	 * 
	 * @return 如果匹配，则返回<code>true</code>
	 */
	private static boolean getOSMatches(String osNamePrefix) {
		if (StringTools.isBlank(getOSName())) {
			return false;
		}

		return getOSName().startsWith(osNamePrefix);
	}
	
	

	/**
	 * 判断当前OS的类型。
	 * 
	 * <p>
	 * 如果不能取得系统属性<code>os.name</code>（因为Java安全限制），则总是返回<code>false</code>
	 * </p>
	 * 
	 * @return 如果当前OS类型为AIX，则返回<code>true</code>
	 */
	public static boolean isAix() {
		return IS_OS_AIX;
	}

	/**
	 * 判断当前OS的类型。
	 * 
	 * <p>
	 * 如果不能取得系统属性<code>os.name</code>（因为Java安全限制），则总是返回<code>false</code>
	 * </p>
	 * 
	 * @return 如果当前OS类型为HP-UX，则返回<code>true</code>
	 */
	public static boolean isHpUx() {
		return IS_OS_HP_UX;
	}

	/**
	 * 判断当前OS的类型。
	 * 
	 * <p>
	 * 如果不能取得系统属性<code>os.name</code>（因为Java安全限制），则总是返回<code>false</code>
	 * </p>
	 * 
	 * @return 如果当前OS类型为IRIX，则返回<code>true</code>
	 */
	public static boolean isIrix() {
		return IS_OS_IRIX;
	}

	/**
	 * 判断当前OS的类型。
	 * 
	 * <p>
	 * 如果不能取得系统属性<code>os.name</code>（因为Java安全限制），则总是返回<code>false</code>
	 * </p>
	 * 
	 * @return 如果当前OS类型为Linux，则返回<code>true</code>
	 */
	public static boolean isLinux() {
		return IS_OS_LINUX;
	}

	/**
	 * 判断当前OS的类型。
	 * 
	 * <p>
	 * 如果不能取得系统属性<code>os.name</code>（因为Java安全限制），则总是返回<code>false</code>
	 * </p>
	 * 
	 * @return 如果当前OS类型为Mac，则返回<code>true</code>
	 */
	public static boolean isMac() {
		return IS_OS_MAC;
	}

	/**
	 * 判断当前OS的类型。
	 * 
	 * <p>
	 * 如果不能取得系统属性<code>os.name</code>（因为Java安全限制），则总是返回<code>false</code>
	 * </p>
	 * 
	 * @return 如果当前OS类型为MacOS X，则返回<code>true</code>
	 */
	public static boolean isMacOsX() {
		return IS_OS_MAC_OSX;
	}

	/**
	 * 判断当前OS的类型。
	 * 
	 * <p>
	 * 如果不能取得系统属性<code>os.name</code>（因为Java安全限制），则总是返回<code>false</code>
	 * </p>
	 * 
	 * @return 如果当前OS类型为OS2，则返回<code>true</code>
	 */
	public static boolean isOs2() {
		return IS_OS_OS2;
	}

	/**
	 * 判断当前OS的类型。
	 * 
	 * <p>
	 * 如果不能取得系统属性<code>os.name</code>（因为Java安全限制），则总是返回<code>false</code>
	 * </p>
	 * 
	 * @return 如果当前OS类型为Solaris，则返回<code>true</code>
	 */
	public static boolean isSolaris() {
		return IS_OS_SOLARIS;
	}

	/**
	 * 判断当前OS的类型。
	 * 
	 * <p>
	 * 如果不能取得系统属性<code>os.name</code>（因为Java安全限制），则总是返回<code>false</code>
	 * </p>
	 * 
	 * @return 如果当前OS类型为Sun OS，则返回<code>true</code>
	 */
	public static boolean isSunOS() {
		return IS_OS_SUN_OS;
	}

	/**
	 * 判断当前OS的类型。
	 * 
	 * <p>
	 * 如果不能取得系统属性<code>os.name</code>（因为Java安全限制），则总是返回<code>false</code>
	 * </p>
	 * 
	 * @return 如果当前OS类型为Windows，则返回<code>true</code>
	 */
	public static boolean isWindows() {
		return IS_OS_WINDOWS;
	}

	/**
	 * 判断当前OS的类型。
	 * 
	 * <p>
	 * 如果不能取得系统属性<code>os.name</code>（因为Java安全限制），则总是返回<code>false</code>
	 * </p>
	 * 
	 * @return 如果当前OS类型为Windows 2000，则返回<code>true</code>
	 */
	public static boolean isWindows2000() {
		return IS_OS_WINDOWS_2000;
	}

	/**
	 * 判断当前OS的类型。
	 * 
	 * <p>
	 * 如果不能取得系统属性<code>os.name</code>（因为Java安全限制），则总是返回<code>false</code>
	 * </p>
	 * 
	 * @return 如果当前OS类型为Windows 95，则返回<code>true</code>
	 */
	public static boolean isWindows95() {
		return IS_OS_WINDOWS_95;
	}

	/**
	 * 判断当前OS的类型。
	 * 
	 * <p>
	 * 如果不能取得系统属性<code>os.name</code>（因为Java安全限制），则总是返回<code>false</code>
	 * </p>
	 * 
	 * @return 如果当前OS类型为Windows 98，则返回<code>true</code>
	 */
	public static boolean isWindows98() {
		return IS_OS_WINDOWS_98;
	}

	/**
	 * 判断当前OS的类型。
	 * 
	 * <p>
	 * 如果不能取得系统属性<code>os.name</code>（因为Java安全限制），则总是返回<code>false</code>
	 * </p>
	 * 
	 * @return 如果当前OS类型为Windows ME，则返回<code>true</code>
	 */
	public static boolean isWindowsME() {
		return IS_OS_WINDOWS_ME;
	}

	/**
	 * 判断当前OS的类型。
	 * 
	 * <p>
	 * 如果不能取得系统属性<code>os.name</code>（因为Java安全限制），则总是返回<code>false</code>
	 * </p>
	 * 
	 * @return 如果当前OS类型为Windows NT，则返回<code>true</code>
	 */
	public static boolean isWindowsNT() {
		return IS_OS_WINDOWS_NT;
	}

	/**
	 * 判断当前OS的类型。
	 * 
	 * <p>
	 * 如果不能取得系统属性<code>os.name</code>（因为Java安全限制），则总是返回<code>false</code>
	 * </p>
	 * 
	 * @return 如果当前OS类型为Windows XP，则返回<code>true</code>
	 */
	public static boolean isWindowsXP() {
		return IS_OS_WINDOWS_XP;
	}

	/**
	 * 取得OS的文件路径的分隔符（取自系统属性：<code>file.separator</code>）。
	 * 
	 * <p>
	 * 例如：Unix为<code>"/"</code>，Windows为<code>"\\"</code>。
	 * </p>
	 * 
	 * @return 属性值，如果不能取得（因为Java安全限制）或值不存在，则返回<code>null</code>。
	 * 
	 * @since Java 1.1
	 */
	public static String getOSFileSeparator() {
		return System.getProperty("file.separator");
	}
	
	/**
	 * 取得OS的文本文件换行符（取自系统属性：<code>line.separator</code>）。
	 * 
	 * <p>
	 * 例如：Unix为<code>"\n"</code>，Windows为<code>"\r\n"</code>。
	 * </p>
	 * 
	 * @return 属性值，如果不能取得（因为Java安全限制）或值不存在，则返回<code>null</code>。
	 * 
	 * @since Java 1.1
	 */
	public static String getOSLineSeparator() {
		return System.getProperty("line.separator");
	}
	
	
	/**
	 * 取得OS的搜索路径分隔符（取自系统属性：<code>path.separator</code>）。
	 * 
	 * <p>
	 * 例如：Unix为<code>":"</code>，Windows为<code>";"</code>。
	 * </p>
	 * 
	 * @return 属性值，如果不能取得（因为Java安全限制）或值不存在，则返回<code>null</code>。
	 * 
	 * @since Java 1.1
	 */
	public static String getOSPathSeparator() {
		return System.getProperty("path.separator");
	}
	
	
	
	
	
	

}
