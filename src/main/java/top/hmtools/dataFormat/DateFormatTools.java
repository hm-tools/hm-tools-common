package top.hmtools.dataFormat;

import java.text.SimpleDateFormat;
import java.util.Date;

import top.hmtools.enums.EDateFormat;

/**
 * 格式化日期的工具类
 * <table border=0 cellspacing=3 cellpadding=0 summary="Chart shows pattern letters, date/time component, presentation, and examples.">
 * <tr bgcolor="#ccccff">
 * <th align=left>字母
 * <th align=left>日期或时间元素
 * <th align=left>表示
 * <th align=left>示例
 * <tr>
 * <td><code>G</code>
 * <td>Era 标志符
 * <td><a href="#text">Text</a>
 * <td><code>AD</code>
 * <tr bgcolor="#eeeeff">
 * <td><code>y</code>
 * <td>年
 * <td><a href="#year">Year</a>
 * <td><code>1996</code>; <code>96</code>
 * <tr>
 * <td><code>M</code>
 * <td>年中的月份
 * <td><a href="#month">Month</a>
 * <td><code>July</code>; <code>Jul</code>; <code>07</code>
 * <tr bgcolor="#eeeeff">
 * <td><code>w</code>
 * <td>年中的周数
 * <td><a href="#number">Number</a>
 * <td><code>27</code>
 * <tr>
 * <td><code>W</code>
 * <td>月份中的周数
 * <td><a href="#number">Number</a>
 * <td><code>2</code>
 * <tr bgcolor="#eeeeff">
 * <td><code>D</code>
 * <td>年中的天数
 * <td><a href="#number">Number</a>
 * <td><code>189</code>
 * <tr>
 * <td><code>d</code>
 * <td>月份中的天数
 * <td><a href="#number">Number</a>
 * <td><code>10</code>
 * <tr bgcolor="#eeeeff">
 * <td><code>F</code>
 * <td>月份中的星期
 * <td><a href="#number">Number</a>
 * <td><code>2</code>
 * <tr>
 * <td><code>E</code>
 * <td>星期中的天数
 * <td><a href="#text">Text</a>
 * <td><code>Tuesday</code>; <code>Tue</code>
 * <tr bgcolor="#eeeeff">
 * <td><code>a</code>
 * <td>Am/pm 标记
 * <td><a href="#text">Text</a>
 * <td><code>PM</code>
 * <tr>
 * <td><code>H</code>
 * <td>一天中的小时数（0-23）
 * <td><a href="#number">Number</a>
 * <td><code>0</code>
 * <tr bgcolor="#eeeeff">
 * <td><code>k</code>
 * <td>一天中的小时数（1-24）
 * <td><a href="#number">Number</a>
 * <td><code>24</code>
 * <tr>
 * <td><code>K</code>
 * <td>am/pm 中的小时数（0-11）
 * <td><a href="#number">Number</a>
 * <td><code>0</code>
 * <tr bgcolor="#eeeeff">
 * <td><code>h</code>
 * <td>am/pm 中的小时数（1-12）
 * <td><a href="#number">Number</a>
 * <td><code>12</code>
 * <tr>
 * <td><code>m</code>
 * <td>小时中的分钟数
 * <td><a href="#number">Number</a>
 * <td><code>30</code>
 * <tr bgcolor="#eeeeff">
 * <td><code>s</code>
 * <td>分钟中的秒数
 * <td><a href="#number">Number</a>
 * <td><code>55</code>
 * <tr>
 * <td><code>S</code>
 * <td>毫秒数
 * <td><a href="#number">Number</a>
 * <td><code>978</code>
 * <tr bgcolor="#eeeeff">
 * <td><code>z</code>
 * <td>时区
 * <td><a href="#timezone">General time zone</a>
 * <td><code>Pacific Standard Time</code>; <code>PST</code>;
 * <code>GMT-08:00</code>
 * <tr>
 * <td><code>Z</code>
 * <td>时区
 * <td><a href="#rfc822timezone">RFC 822 time zone</a>
 * <td><code>-0800</code>
 * </table>
 * 
 * @author HyboJ
 * 
 */
public class DateFormatTools {

	/**
	 * 格式化日期为字符串
	 * 
	 * @param date
	 * @param dateFormat
	 * @return
	 */
	public static String getFormatDatetime(Date date, EDateFormat dateFormat) {
		return getFormatDatetime(date, dateFormat.toString());
	}

	/**
	 * 格式化日期为字符串
	 * <table border=0 cellspacing=3 cellpadding=0 summary="Chart shows pattern letters, date/time component, presentation, and examples.">
	 * <tr bgcolor="#ccccff">
	 * <th align=left>字母
	 * <th align=left>日期或时间元素
	 * <th align=left>表示
	 * <th align=left>示例
	 * <tr>
	 * <td><code>G</code>
	 * <td>Era 标志符
	 * <td><a href="#text">Text</a>
	 * <td><code>AD</code>
	 * <tr bgcolor="#eeeeff">
	 * <td><code>y</code>
	 * <td>年
	 * <td><a href="#year">Year</a>
	 * <td><code>1996</code>; <code>96</code>
	 * <tr>
	 * <td><code>M</code>
	 * <td>年中的月份
	 * <td><a href="#month">Month</a>
	 * <td><code>July</code>; <code>Jul</code>; <code>07</code>
	 * <tr bgcolor="#eeeeff">
	 * <td><code>w</code>
	 * <td>年中的周数
	 * <td><a href="#number">Number</a>
	 * <td><code>27</code>
	 * <tr>
	 * <td><code>W</code>
	 * <td>月份中的周数
	 * <td><a href="#number">Number</a>
	 * <td><code>2</code>
	 * <tr bgcolor="#eeeeff">
	 * <td><code>D</code>
	 * <td>年中的天数
	 * <td><a href="#number">Number</a>
	 * <td><code>189</code>
	 * <tr>
	 * <td><code>d</code>
	 * <td>月份中的天数
	 * <td><a href="#number">Number</a>
	 * <td><code>10</code>
	 * <tr bgcolor="#eeeeff">
	 * <td><code>F</code>
	 * <td>月份中的星期
	 * <td><a href="#number">Number</a>
	 * <td><code>2</code>
	 * <tr>
	 * <td><code>E</code>
	 * <td>星期中的天数
	 * <td><a href="#text">Text</a>
	 * <td><code>Tuesday</code>; <code>Tue</code>
	 * <tr bgcolor="#eeeeff">
	 * <td><code>a</code>
	 * <td>Am/pm 标记
	 * <td><a href="#text">Text</a>
	 * <td><code>PM</code>
	 * <tr>
	 * <td><code>H</code>
	 * <td>一天中的小时数（0-23）
	 * <td><a href="#number">Number</a>
	 * <td><code>0</code>
	 * <tr bgcolor="#eeeeff">
	 * <td><code>k</code>
	 * <td>一天中的小时数（1-24）
	 * <td><a href="#number">Number</a>
	 * <td><code>24</code>
	 * <tr>
	 * <td><code>K</code>
	 * <td>am/pm 中的小时数（0-11）
	 * <td><a href="#number">Number</a>
	 * <td><code>0</code>
	 * <tr bgcolor="#eeeeff">
	 * <td><code>h</code>
	 * <td>am/pm 中的小时数（1-12）
	 * <td><a href="#number">Number</a>
	 * <td><code>12</code>
	 * <tr>
	 * <td><code>m</code>
	 * <td>小时中的分钟数
	 * <td><a href="#number">Number</a>
	 * <td><code>30</code>
	 * <tr bgcolor="#eeeeff">
	 * <td><code>s</code>
	 * <td>分钟中的秒数
	 * <td><a href="#number">Number</a>
	 * <td><code>55</code>
	 * <tr>
	 * <td><code>S</code>
	 * <td>毫秒数
	 * <td><a href="#number">Number</a>
	 * <td><code>978</code>
	 * <tr bgcolor="#eeeeff">
	 * <td><code>z</code>
	 * <td>时区
	 * <td><a href="#timezone">General time zone</a>
	 * <td><code>Pacific Standard Time</code>; <code>PST</code>;
	 * <code>GMT-08:00</code>
	 * <tr>
	 * <td><code>Z</code>
	 * <td>时区
	 * <td><a href="#rfc822timezone">RFC 822 time zone</a>
	 * <td><code>-0800</code>
	 * </table>
	 * @param date
	 * @param dateFormat  参考本注释上方表格
	 * @return
	 */
	public static String getFormatDatetime(Date date, String dateFormat) {
		SimpleDateFormat sdf = new SimpleDateFormat(dateFormat.toString());
		String result = sdf.format(date);
		return result;
	}
}
