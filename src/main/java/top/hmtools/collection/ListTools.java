package top.hmtools.collection;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import top.hmtools.base.StringTools;

/**
 * 操作List集合的相关工具方法
 * @author Jianghaibo
 *
 */
public class ListTools {

    /**
     * 将数组转换为ArrayList，支持泛型。
    * 方法说明：                    getArrayList
    * 输入参数说明：           @param array
    * 输入参数说明：           @return
    * 输出参数说明：           List
    *
    *
     */
    public static <T> ArrayList<T> getArrayList(T[] array){
        if(array == null || array.length<1){
            return null;
        }
        ArrayList<T> result = new ArrayList<T>();
        for(T t:array){
            result.add(t);
        }
        return result;
    }
    
    /**
     * 将字符串按指定分隔符分隔后，获取其ArrayList集合
    * 方法说明：                    getArrayList
    * 输入参数说明：           @param src
    * 输入参数说明：           @param split
    * 输入参数说明：           @return
    * 输出参数说明：           ArrayList<String>
    *
    *
     */
    public static ArrayList<String> getArrayList(String src,String split){
        if(StringTools.isBlank(src)){
            return null;
        }
        String[] strings = src.split(split);
        return getArrayList(strings);
    }
    
    /**
     * 获取去重元素后的ArrayList
    * 方法说明：                    removeDuplicate
    * 输入参数说明：           @param src
    * 输入参数说明：           @return
    * 输出参数说明：           ArrayList<T>
    *
    *
     */
    public static <T>ArrayList<T> removeDuplicate(List<T> src){
        if(src == null || src.size()<1){
            return null;
        }
        HashSet<T> set = new HashSet<T>();
        set.addAll(src);
        ArrayList< T> result = new ArrayList<T>();
        result.addAll(set);
        return result;
    }
    
    /**
     * 获取多个List去重后的并集ArrayList集合（支持泛型）
    * 方法说明：                    union
    * 输入参数说明：           @param lists
    * 输入参数说明：           @return
    * 输出参数说明：           ArrayList<T>
    *
    *
     */
    public static <T>ArrayList<T> union(List<T>...lists){
        if(lists ==null || lists.length<1){
            return null;
        }
        HashSet<T> set = new HashSet<T>();
        for(List<T> list:lists){
            set.addAll(list);
        }
        
        ArrayList< T> result = new ArrayList<T>();
        result.addAll(set);
        return result;
    }
    
    /**
     * 获取多个List的交集元素（没有去重）
    * 方法说明：                    retainAll
    * 输入参数说明：           @param lists
    * 输入参数说明：           @return
    * 输出参数说明：           List<T>
    *
    *
     */
    public static <T> List<T> retainAll(List<T>...lists){
        if(lists ==null || lists.length<1){
            return null;
        }
        
        List<T> first = lists[0];
        for(List<T> list:lists){
            first.retainAll(list);
        }
        return first;
    }
    
    /**
     * 获取多个List的交集元素（去重后）
    * 方法说明：                    retainAllAndRemoveDuplicate
    * 输入参数说明：           @param lists
    * 输入参数说明：           @return
    * 输出参数说明：           List<T>
    *
    *
     */
    public static <T> List<T> retainAllAndRemoveDuplicate(List<T>...lists){
        return removeDuplicate(retainAll(lists));
    }

    /**
     * 打印list中的所有元素
     * @param list
     */
	public static<T> void println(List<T> list) {
		if(list != null){
			for(T tt:list){
				System.out.println(tt);
			}
		}
	}
    
    
    
    
    
    
    
    
    
    
    
}
