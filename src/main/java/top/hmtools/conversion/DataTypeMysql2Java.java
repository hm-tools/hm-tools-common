package top.hmtools.conversion;

import java.util.HashMap;
import java.util.Map;

import top.hmtools.base.StringTools;

/**
 * MySQL数据类型转Java数据类型工具
 * @author HyboJ
 *
 */
public class DataTypeMysql2Java {

	private static Map<String,String> DIC;
	
	static {
		DIC = new HashMap<String,String>();
		DIC.put("VARCHAR","java.lang.String");
		DIC.put("CHAR","java.lang.String");
		DIC.put("BLOB","java.lang.byte[]");
		DIC.put("TEXT","java.lang.String");
		DIC.put("INTEGER","java.lang.Long");
		DIC.put("TINYINT","java.lang.Integer");
		DIC.put("INT","java.lang.Integer");
		DIC.put("SMALLINT","java.lang.Integer");
		DIC.put("MEDIUMINT","java.lang.Integer");
		DIC.put("BIT","java.lang.Boolean");
		DIC.put("BIGINT","java.math.BigInteger");
		DIC.put("FLOAT","java.lang.Float");
		DIC.put("DOUBLE","java.lang.Double");
		DIC.put("DECIMAL","java.math.BigDecimal");
		DIC.put("BOOLEAN","java.lang.Boolean");
		DIC.put("ID","java.lang.Long");
		DIC.put("DATE","java.sql.Date");
		DIC.put("TIME","java.sql.Time");
		DIC.put("DATETIME","java.sql.Timestamp");
		DIC.put("TIMESTAMP","java.sql.Timestamp");
		DIC.put("YEAR","java.sql.Date");
	}
	
	/**
	 * 根据MySQL数据类型名称获取简短名称的Java数据对象类型
	 * @param mysqlName
	 * @return
	 */
	public static String getShortJavaName(String mysqlName){
		String fullJavaName = getFullJavaName(mysqlName);
		if(StringTools.isBlank(fullJavaName)){
			return null;
		}else{
			int lastIndexOf = fullJavaName.lastIndexOf(".");
			return fullJavaName.substring(lastIndexOf+1);
		}
	}
	
	/**
	 * 根据MySQL数据类型名称获取完全限定名称的Java数据对象类型
	 * @param mysqlName
	 * @return
	 */
	public static String getFullJavaName(String mysqlName){
		if(StringTools.isBlank(mysqlName)){
			return null;
		}
		return DIC.get(mysqlName.trim().toUpperCase());
	}
}
