package top.hmtools.conversion;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import top.hmtools.base.StringTools;
import top.hmtools.io.IoTools;

/**
 * 文件扩展名与MIME名转换工具
 * @author HyboJ
 *
 */
public class Extension2Mime {
	
	/**
	 * 当前要读取的资源文件名称
	 */
	private static String contentFileName = "MimeDatas/MIME2018.txt";
	
	/**
	 * 文件扩展名为key，MIME为值的字典
	 */
	private static Map<String,String> E2M_DIC = new HashMap<String,String>();
	
	/**
	 * MIME为key，文件扩展名为值的字典
	 */
	private static Map<String,String> M2E_DIC = new HashMap<String, String>();
	
	/**
	 * 原始记录集合
	 */
	private static List<String> contentLines;

	/**
	 * 分隔符
	 */
	private static String regex=",";
	
	/**
	 * 初始化数据
	 */
	static {
		init();
	}
	
	/**
	 * 初始化数据
	 */
	public static void init(){
		InputStream inputStream = Thread.currentThread().getContextClassLoader().getResourceAsStream(contentFileName);
		try {
			contentLines = IoTools.readLineToStringListUTF8(inputStream);
			
			for(String line:contentLines){
				String[] item = line.split("=");
				if(item.length!=2){
					continue;
				}
				String extension = item[0];
				String mime = item[1];
				
				E2M_DIC.put(extension, mime);
				if(M2E_DIC.containsKey(mime)){
					String tmp = M2E_DIC.get(mime);
					M2E_DIC.put(mime, tmp+regex+extension);
				}else{
					M2E_DIC.put(mime, extension);
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * 根据文件扩展名获取对应的MIME
	 * @param Extension
	 * @return
	 */
	public static String getMimeByExtension(String Extension){
		return E2M_DIC.get(Extension);
	}
	
	/**
	 * 根据文件扩展名获取对应的MIME集合（模糊查询）
	 * @param keyExtension
	 * @return
	 */
	public static String[] getMimesByExtensionLike(String keyExtension){
		List<String> result = new ArrayList<String>();
		for(String ext : E2M_DIC.keySet()){
			if(ext.contains(keyExtension)){
				result.add(E2M_DIC.get(ext));
			}
		}
		return result.toArray(new String[0]);
	}
	
	/**
	 * 根据MIME获取对应的文件扩展名集合
	 * @param mime
	 * @return
	 */
	public static String[] getExtensionByMime(String mime){
		String value = M2E_DIC.get(mime);
		if(StringTools.isBlank(value)){
			return null;
		}else{
			return value.split(regex);
		}
	}
	
	

	/**
	 * 打印所有内容
	 */
	public static void printContent(){
		InputStream inputStream = Thread.currentThread().getContextClassLoader().getResourceAsStream(contentFileName);
		try {
			String stringUTF8 = IoTools.readToStringUTF8(inputStream);
			System.out.println(stringUTF8);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public static void main(String args[]){
		printContent();
	}
}
