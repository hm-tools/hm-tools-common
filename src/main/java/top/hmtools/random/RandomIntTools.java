package top.hmtools.random;

import java.util.Random;

/**
 * 随机整数工具类
 * @author Jianghaibo
 *
 */
public class RandomIntTools {

    /**
     * 获取指定范围的随机整数
    * 方法说明：                    getIntByRange
    * 输入参数说明：           @param min
    * 输入参数说明：           @param max
    * 输入参数说明：           @return
    * 输出参数说明：           int
    *
    *
     */
    public static int getIntByRange(int min,int max){
        Random random = new Random();
        int result = random.nextInt(max)%(max-min+1) + min;
        return result;
    }
    
    /**
     * 获取0~max范围内的随机整数
    * 方法说明：                    getIntByMax
    * 输入参数说明：           @param max
    * 输入参数说明：           @return
    * 输出参数说明：           int
    *
    *
     */
    public static int getIntByMax(int max){
        return getIntByRange(0, max);
    }
}
