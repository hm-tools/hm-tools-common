package top.hmtools.annotation;

import org.junit.Test;

import top.hmtools.model.ControllerInfoModel;

/**
 * 测试解析 controllerInfo注解
 * @author HyboJ
 *
 */
public class ControllerInfoTest {

	@Test
    public void testGetControllerInfo() {
		//解析修饰类的controllerInfo注解
    	ControllerInfoModel controllerInfoOfClass = ControllerInfoTools.getControllerInfoOfClass(ExampleController.class);
    	System.out.println(controllerInfoOfClass);
    	
    	//解析修饰方法的controllerInfo注解
    	ControllerInfoModel controllerInfoOfMethod = ControllerInfoTools.getControllerInfoOfMethod(ExampleController.class.getMethods()[0]);
    	System.out.println(controllerInfoOfMethod);
    }

}
