package top.hmtools.annotation;

import top.hmtools.enums.EDataFormat;

/**
 * 示例controller
 * 
 * @author HyboJ
 * 
 */
@ControllerInfo(name = "示例controller", usedFor = "用于测试controllerInfo注解")
public class ExampleController {

	@ControllerInfo(name = "新增用户", inputDataFormat = EDataFormat.TEXT, usedFor = "新增一个用户信息", paramInfos = {
			@ParamInfo(paramName = "userName", comment = "用户名信息"),
			@ParamInfo(paramName = "password", comment = "密码信息") },ResultInfos={@ResultInfo(resultName="status",comment="状态")})
	public void add(String userName, String password) {

	}

}
