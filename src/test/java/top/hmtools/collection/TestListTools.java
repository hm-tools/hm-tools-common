package top.hmtools.collection;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

public class TestListTools {

    @Test
    public void testGetArrayList(){
        String aaa = "aaa,bbb,ccc,ddd,eee";
        ArrayList<String> arrayList = ListTools.getArrayList(aaa, ",");
        System.out.println(arrayList);
    }
    
    @Test
    public void testremoveDuplicate(){
        ArrayList<String> list = new ArrayList<String>();
        list.add("aa");
        list.add("bb");
        list.add("cc");
        list.add("aa");
        list.add("aa");
        list.add("dd");
        list.add("ee");
        list.add("ff");
        list.add("dd");
        
        ArrayList<String> removeDuplicate = ListTools.removeDuplicate(list);
        System.out.println(removeDuplicate);
    }
    
    @SuppressWarnings("unchecked")
    @Test
    public void testunion(){
        
        ArrayList<String> listb = new ArrayList<String>();
        listb.add("gg");
        listb.add("hh");
        listb.add("yy");
        listb.add("aa");
        listb.add("aa");
        listb.add("dd");
        listb.add("ee");
        listb.add("ff");
        listb.add("dd");

        ArrayList<String> list = new ArrayList<String>();
        list.add("aa");
        list.add("bb");
        list.add("cc");
        list.add("aa");
        list.add("aa");
        list.add("dd");
        list.add("ee");
        list.add("ff");
        list.add("dd");
        
        ArrayList<String> union = ListTools.union(list,listb);
        System.out.println(union);
    }
    
    @SuppressWarnings("unchecked")
    @Test
    public void testretainAll(){
        ArrayList<String> listb = new ArrayList<String>();
        listb.add("gg");
        listb.add("hh");
        listb.add("yy");
        listb.add("aa");
        listb.add("aa");
        listb.add("dd");
        listb.add("ee");
        listb.add("ff");
        listb.add("dd");

        ArrayList<String> list = new ArrayList<String>();
        list.add("aa");
        list.add("bb");
        list.add("cc");
        list.add("aa");
        list.add("aa");
        list.add("dd");
        list.add("ee");
        list.add("ff");
        list.add("dd");
        
        List<String> retainAll = ListTools.retainAll(listb,list);
        System.out.println(retainAll);
        
        retainAll = ListTools.retainAllAndRemoveDuplicate(listb,list);
        System.out.println(retainAll);
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
}
