package top.hmtools.io;

import java.net.MalformedURLException;
import java.util.HashMap;
import java.util.Map;

import org.junit.Test;

/**
 * 测试文件名称工具
 * @author Jianghaibo
 *
 */
public class FileNameToolsTest {

    @Test
    public void testAA() throws MalformedURLException{
    	String url = "jdbc:mysql://127.0.0.1:3306/magpiecp?characterEncoding=utf8&zeroDateTimeBehavior=convertToNull";
    	String baseName = FileNameTools.getBaseName(url);
    	System.out.println(baseName);
    }
    
    @Test
    public void testBBb(){
    	String filename = "a123456.jpg";
    	Map<String,String> info = new HashMap<String,String>();
    	info.put("md5", "mmmdddd5555");
    	info.put("fileId", "20180101");
    	String newFilename = FileNameTools.saveInifo2FileName(info, filename);
    	System.out.println(newFilename);
    	Map<String, String> infoFromFileName = FileNameTools.getInfoFromFileName(newFilename);
    	System.out.println(infoFromFileName);
    	String originalNameByInfoName = FileNameTools.getOriginalNameByInfoName(newFilename);
    	System.out.println(originalNameByInfoName);
    }
}
