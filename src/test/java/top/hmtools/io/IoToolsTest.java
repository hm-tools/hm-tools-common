package top.hmtools.io;

import java.io.FileInputStream;

import org.junit.Test;

/**
 * 测试 iotools
 * @author HyboJ
 *
 */
public class IoToolsTest {

	/**
	 * 测试读取流到字符串，并以utf-8编码
	 */
	@Test
	public void testReadToStringUTF8(){
		try {
			FileInputStream fis = new FileInputStream("D:\\utf-8.txt");
			String readToStringUTF8 = IoTools.readToStringUTF8(fis);
			System.out.println(readToStringUTF8);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void testreadToStringHex(){
		try {
			FileInputStream fis = new FileInputStream("D:\\utf-8.txt");
			String readToStringUTF8 = IoTools.readToStringHex(fis, 20, false);
			System.out.println(readToStringUTF8);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
