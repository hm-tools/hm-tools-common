package top.hmtools.security;

import java.util.Map;

import org.junit.Test;

public class RSAToolsTest {

	/**
	 * 测试base64编码解码
	 */
	@Test
	public void testBase64(){
		String str = "你好啊！~我爱吃菠萝！";
		byte[] bytes = str.getBytes();
		String encode = RSATools.encodeBASE64(bytes);
		byte[] decode = RSATools.decodeBASE64(encode);
		System.out.println(new String(decode));
	}
	
	/**
	 * 测试获取RSA公钥私钥对
	 */
	@Test
	public void testGetRSAKeies(){
		Map<String, String> generateKeyBase64Str = RSATools.getGenerateKeyBase64Str(2048);
		System.out.println(generateKeyBase64Str);
	}
}
