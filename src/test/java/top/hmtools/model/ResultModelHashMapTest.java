package top.hmtools.model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.junit.Test;

import top.hmtools.model.ResultModel.keyNames;
import top.hmtools.model.ResultModel.resultStatus;

/**
 * 测试ResultModelHashmap
 * @author Jianghaibo
 *
 */
public class ResultModelHashMapTest {
    
    /**
     * 测试ResultModelHashmap构造函数
    * 方法说明：                    testAA
    * 输入参数说明：           
    * 输出参数说明：           void
    *
    *
     */
    @Test
    public void testResultModel(){
        //初始化一个结果模型
        System.out.println("初始化一个结果模型：");
        ResultModel<String> rm = new ResultModel<String>();
        System.out.println(rm);
        
        //写入数据集
        System.out.println("写入数据集：");
        List<String> aaa = new ArrayList<String>();
        aaa.add("111");
        aaa.add("222");
        aaa.add("333");
        rm.setRows(aaa);
        System.out.println(rm.toString());
        
        //设置执行结果状态
        System.out.println("执行结果状态：");
        rm.setStatus(resultStatus.STATUS_SUCCESS);
        System.out.println(rm.toString());
        
        //测试执行结果状态枚举
        System.out.println("执行结果状态枚举：");
        resultStatus[] statuses = resultStatus.values();
        for(resultStatus status:statuses){
            System.out.println(status);
        }
        
        //追加数据集
        System.out.println("追加数据集");
        System.out.println(rm.toString());
    }
    
    @Test
    public void testAddToRows(){
    	ResultModel<String> rm = new ResultModel<String>(resultStatus.STATUS_FAIL);
    	rm.setRows("aaa","bbb","cccc");
    	rm.addToRows("哈哈");
    	System.out.println(rm.toString());
    	
    	List<String> rows = rm.getRows();
    	
    	System.out.println(rows);
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
}
