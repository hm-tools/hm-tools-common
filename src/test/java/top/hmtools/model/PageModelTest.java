package top.hmtools.model;

import org.junit.Test;

/**
 * 测试分页模型
 * @author Pucou
 *
 */
public class PageModelTest {

	@Test
	public void testPageModelAA(){
		PageModel model = new PageModel(19);
		model.printlnInfo();
		
		model.setCurrPageNo(2).setPageSize(10).init();
		model.printlnInfo();
	}
}
