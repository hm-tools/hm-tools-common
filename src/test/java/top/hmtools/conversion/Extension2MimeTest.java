package top.hmtools.conversion;

import java.util.Arrays;

import org.junit.Test;

public class Extension2MimeTest {

	@Test
	public void testAAA(){
		Extension2Mime.printContent();
	}
	
	@Test
	public void testBBB(){
		String extension = Extension2Mime.getMimeByExtension("jpg");
		System.out.println(extension);
	}
	
	@Test
	public void testCCC(){
		String[] mime = Extension2Mime.getExtensionByMime("image/jpeg");
		System.out.println(Arrays.toString(mime));
	}
	
	@Test
	public void testDDD(){
		String[] mimes = Extension2Mime.getMimesByExtensionLike("j");
		System.out.println(Arrays.toString(mimes));
	}
	
	public static void main(String args[]){
		Extension2Mime.printContent();
	}
}
