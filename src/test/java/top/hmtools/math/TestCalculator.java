package top.hmtools.math;

import java.util.Stack;

import org.junit.Test;

import top.hmtools.Math.Calculator;

public class TestCalculator {

    @Test
    public void testconversion() {
        double conversion = Calculator.conversion("(1+3)/(2*4)");
        System.out.println(conversion);
    }

    /**
     * 测试括号是否哦左右呼应
    * 方法说明：                    isGoodBracket
    * 输入参数说明：           @param s
    * 输入参数说明：           @return
    * 输出参数说明：           boolean
    *
    *
     */
    public static boolean isGoodBracket(String s) {
        Stack<Character> a = new Stack<Character>();
        for (int i = 0; i < s.length(); i++) {
            char c = s.charAt(i);
            if (c == '(')
                a.push(')');
            if (c == '[')
                a.push(']');
            if (c == '{')
                a.push('}');
            if (c == ')' || c == ']' || c == '}') {
                if (a.size() == 0)
                    return false; // 填空
                if (a.pop() != c)
                    return false;
            }
        }
        if (a.size() != 0)
            return false; // 填空
        return true;
    }
    
    @Test
    public  void testIsGoodBracket() {
        System.out.println(isGoodBracket("...(..[.)..].{.(..).}..."));
        System.out.println(isGoodBracket("...(..[...].(.).).{.(..).}..."));
        System.out.println(isGoodBracket(".....[...].(.).){.(..).}..."));
        System.out.println(isGoodBracket("...(..[...].(.).){.(..)...."));
    }
}
