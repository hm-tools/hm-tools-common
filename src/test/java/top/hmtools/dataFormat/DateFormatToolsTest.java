package top.hmtools.dataFormat;

import java.util.Date;

import org.junit.Test;

import top.hmtools.enums.EDateFormat;

/**
 * 测试日期格式化
 * @author HyboJ
 *
 */
public class DateFormatToolsTest {

	@Test
	public void testDateFormat(){
		String datetime = DateFormatTools.getFormatDatetime(new Date(), EDateFormat.yyyyMMdd_hg_HHmmss_m_SSS);
		System.out.println(datetime);
		
		datetime = DateFormatTools.getFormatDatetime(new Date(), EDateFormat.yyyyMMdd_nyr);
		System.out.println(datetime);
	}
}
