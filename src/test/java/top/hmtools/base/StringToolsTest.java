package top.hmtools.base;

import java.util.List;

import org.junit.Test;

import top.hmtools.collection.ListTools;

public class StringToolsTest {

    @Test
    public void testaa(){
        boolean blank = StringTools.isBlank("","","");
        System.out.println(blank);
        
        boolean notBlank = StringTools.isNotBlank("", "dd","dd");
        System.out.println(notBlank);
        
        boolean isAllBlank = StringTools.isAllBlank("","","");
        System.out.println(isAllBlank);
        
        isAllBlank = StringTools.isAllBlank(null);
        System.out.println(isAllBlank);
        
        isAllBlank = StringTools.isAllBlank("aa","bb","cc");
        System.out.println(isAllBlank);
        
        isAllBlank = StringTools.isAllBlank("aa","bb","");
        System.out.println(isAllBlank);
    }
    
    @Test
    public void testCCC(){
    	String str = "ab2cde2faahfu33awncz2fjsk;huaewy2hrfnskwefajask";
    	List<String> distillRegex = StringTools.distillRegex(str, "\\d+");
    	ListTools.println(distillRegex);
    }
    
    @Test
    public void testBB(){
    	String str = "abcdef..............";
    	String trimSuffix = StringTools.trimSuffix(str, ".");
    	System.out.println(trimSuffix);
//    	System.out.println(trimEnd(str, "."));
    }
    
    private String trimEnd(String str,String suffix){
    	String result = null;
    	if(str.endsWith(suffix)){
    		int lastIndexOf = str.lastIndexOf(suffix);
    		result = str.substring(0,lastIndexOf);
    		result = this.trimEnd(result, suffix);
    	}else{
    		result =  str;
    	}
    	return result;
    }
}
