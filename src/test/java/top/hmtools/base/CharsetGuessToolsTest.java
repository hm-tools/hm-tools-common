package top.hmtools.base;

import java.io.UnsupportedEncodingException;
import java.math.BigInteger;

import org.junit.Test;

public class CharsetGuessToolsTest {

	@Test
	public void testDoGuess() throws UnsupportedEncodingException{
		String str = "abcdefg哇哈哈";
		byte[] bytesUTF8 = str.getBytes("UTF-8");
		System.out.println(binary(bytesUTF8, 16));
		String doGuess = CharsetGuessTools.doGuess(bytesUTF8);
		System.out.println(doGuess);
		
		byte[] bytesGBK = str.getBytes("GBK");
		System.out.println(binary(bytesGBK, 16));
		doGuess = CharsetGuessTools.doGuess(bytesGBK);
		System.out.println(doGuess);
		
		byte[] bytesGB2312 = str.getBytes("GB2312");
		System.out.println(binary(bytesGB2312, 16));
		doGuess = CharsetGuessTools.doGuess(bytesGB2312);
		System.out.println(doGuess);
	}
	
	/**
	 * 将字节码数组转换为指定的进制字符串
	 * @param bytes
	 * @param radix
	 * @return
	 */
	private static String binary(byte[] bytes, int radix){  
        return new BigInteger(1, bytes).toString(radix);// 这里的1代表正数  
    }  
}
