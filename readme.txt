20171121
	重构的hm-tools
	修正top.hmtools.random.RandomStringTools.getGenerateShortUuid(int)指定长度大于8时报错的bug。

20171122
	创建新的resultModel数据模型（业务处理结果统一数据模型），基于hashmap。top.hmtools.model.ResultModelHashmap<K extends String, V extends Object>
	
20171125
	1、新增base64加密解密工具。
	2、新增maven SCM 插件，主要操作maven命令有：
		mvn scm:checkin -Dmessage="代码提交日志" #代码提交
		mvn scm:update #代码更新
		
	3、新增maven release插件，主要操作maven命令有：
		mvn release:clean #发布前的清理
		mvn release:prepare #发布版本
		mvn release:rollback #回滚版本
	4、增获取操作系统信息工具类
	
20171127
	发布jar包到maven中央仓库成功。
	
20171223
	1、io工具类，nio工具类的方法完善
	2、解析controllerInfo注解（用于描述controller信息）
	3、日期格式化成字符串工具